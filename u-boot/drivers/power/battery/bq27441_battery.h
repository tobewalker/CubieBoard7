/*
* TI BQ27441 battery driver
*
 * Copyright (c) 2015 Actions Semiconductor Co., Ltd.
 * Terry Chen chenbo@actions-semi.com
*
 * SPDX-License-Identifier:	GPL-2.0+
 */
#ifndef __BQ27441_BATTERY_H__
#define __BQ27441_BATTERY_H__

int bq27441_battery_init(const void *blob);
#endif

