/*
* Actions ATC2603C PMIC charger driver
*
 * Copyright (c) 2015 Actions Semiconductor Co., Ltd.
 * Terry Chen chenbo@actions-semi.com
*
 * SPDX-License-Identifier:	GPL-2.0+
 */
#ifndef __ATC2603C_CHARGER_H__
#define __ATC2603C_CHARGER_H__

int atc2603c_charger_init(const void *blob);
#endif
