/*
* Actions ATC2609A PMIC charger driver
*
 * Copyright (c) 2015 Actions Semiconductor Co., Ltd.
 * Terry Chen chenbo@actions-semi.com
*
 * SPDX-License-Identifier:	GPL-2.0+
 */
#ifndef __ATC2609A_CHARGER_H__
#define __ATC2609A_CHARGER_H__

void atc2609a_charger_init(const void *blob);
#endif
