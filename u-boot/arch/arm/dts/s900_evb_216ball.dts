/*
 * Copyright (c) 2015 Actions Semi Co., Ltd.
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

/dts-v1/;

#include "s900.dtsi"

/ {
	model = "s900 evb 216ball";
	compatible = "actions,s900_evb_216ball", "actions,s900";

	chosen {
		stdout-path = &serial5;
	};

	clock:e0160000 {
		compatible = "actions,owl-clk";
		core_pll = <1104>;
		dev_pll = <660>;
		display_pll = <1056>;
	};

	pwm: pwm@e01b0000 {
		pwm2 {
			id = <2>;
			mfp = <0>;
		};
	};

	edp@e02e0000 {
		panel_configs = <&config0>;

		/*
		 * edp_configs, used to configure edp controller,
		 * its values should set according to edp panel's spec.
		 * Currently, the configration is for lg-lp097qx1 panel.
		 */
		config0: edp_configs {
			link_rate = <0>;	/* 0, 1.62G; 1, 2.7G; 2, 5.4G */

			lane_count = <4>;
			lane_polarity = <0x00>;	/* bit 0~3 map for lanes' polarity,
						   bit 4 map for aux lanes' polarity,
						   0 normal, 1 reversed */
			lane_mirror = <1>;	/* lane mirror, 0 lane0~3, 1 lane3~0 */

			mstream_polarity = <0x3>;/* bit map for mstream's polarity,
						   bit1: vsync, bit0 hsync */
			user_sync_polarity = <0xc>;/* bit map for user sync's polarity,
						    * bit3: sync signal polarity,
						    * bit2: enable signal polarity,
						    * bit1: vsync polarity,
						    * bit0: hsync polarity
						    */

			pclk_parent = <0>;	/* 0, ASSIST_PLL, 1, DISPLAY PLL */
			pclk_rate = <200000000>;/* 200MHz, ASSIST_PLL, divider 2.5 */
		};

		/* lg-lp097qx1 edp panel */
		panel_gep: edp_panel {
			compatible = "actions,panel-gep";

			/* panel's fixed info */
			width_mm = <197>;
			height_mm = <147>;
			bpp = <18>;

			is_primary = <1>;

			/* operation delay in ms */
			power_on_delay = <0>;
			power_off_delay = <0>;
			enable_delay = <0>;
			disable_delay = <0>;

			/* GPIOA23, active low */
			power-gpio = <&gpioa 23 1>;

			videomode-0 = <&mode0>;

			/* 2048x1536p60 */
			mode0: videomode {
				refresh_rate = <60>;

				xres = <2048>;
				yres = <1536>;

				/*
				 * pclk_rate = 200MHz, ASSIS_PLL, divider 2.5
				 * pixel_clock = picoseconds / pclk_rate
				 */
				pixel_clock = <5000>;

				hsw = <5>;
				hbp = <44>;
				hfp = <48>;

				vsw = <10>;
				vbp = <6>;
				vfp = <2>;

				/* 0: FB_VMODE_NONINTERLACED, 1:FB_VMODE_INTERLACED */
				vmode = <0>;
			};
		};
	};

	i2c0: i2c@e0170000 {
		hdmi_edid {
			compatible = "actions,hdmi-edid";
		};
	};

	i2c3: i2c@e0176000 {
		atc2609a_pmic {
			compatible = "actions,atc2609a";
			reg = <0x65>;

			atc2609a_rtc {
					compatible = "actions,atc2609a-rtc";
			};

			atc2609a_pstore {
					compatible = "actions,atc2609a-pstore";
			};

			atc2609a_misc {
					compatible = "actions,atc2609a-misc";
			};

			atc2609a_auxadc {
					compatible = "actions,atc2609a-auxadc";
			};

			voltage-regulators {

			/* DCDC0       VDD_CORE
			 * DCDC1       VDD_CPU
			 * DCDC2       VDDR
			 * DCDC3       VCC
			 * DCDC4       VDD_CPUM
			 */
			dcdc0: dcdc0 {
				regulator-name = "dcdc0";
				regulator-min-microvolt = <925000>;
				regulator-max-microvolt = <925000>;
				regulator-always-on;
			};

			dcdc1: dcdc1 {
				regulator-name = "dcdc1";
				regulator-min-microvolt = <925000>;
				regulator-max-microvolt = <925000>;
				regulator-always-on;
			};

			dcdc3: dcdc3 {
				regulator-name = "dcdc3";
				regulator-min-microvolt = <3100000>;
				regulator-max-microvolt = <3100000>;
				regulator-always-on;
			};

			dcdc4: dcdc4 {
				regulator-name = "dcdc4";
				regulator-min-microvolt = <925000>;
				regulator-max-microvolt = <925000>;
				regulator-always-on;
			};

			/* LDO0        SD_VCC     
			 * LDO1        SENS_AVDD  
			 * LDO2        AVCC       
			 * LDO3        ATC2609_VDD   
			 * LDO4        AVCC_1V8   
			 * LDO6        AVDD_1V0   
			 * LDO7        SENS_DVDD  
			 * LDO8        TP_VCC     
			 * LDO9        RTC_VDD     */

			ldo0: ldo0{
				regulator-name = "ldo0";
				regulator-min-microvolt  = <3100000>;
				regulator-max-microvolt = <3100000>;
				regulator-always-on;
			};

			ldo1: ldo1{
				regulator-name = "ldo1";
				regulator-min-microvolt  = <2300000>;
				regulator-max-microvolt = <3400000>;
			};

			ldo2: ldo2{
				regulator-name = "ldo2";
				regulator-min-microvolt  = <3100000>;
				regulator-max-microvolt = <3100000>;
				regulator-always-on;
			};

			ldo3: ldo3{
				regulator-name = "ldo3";
				regulator-min-microvolt  = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};

			ldo4: ldo4{
				regulator-name = "ldo4";
				regulator-min-microvolt  = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-always-on;
			};


			ldo6: ldo6{
				regulator-name = "ldo6";
				regulator-min-microvolt  = <1100000>;
				regulator-max-microvolt = <1100000>;
				regulator-always-on;
			};

			ldo7: ldo7{
				regulator-name = "ldo7";
				regulator-min-microvolt  = <700000>;
				regulator-max-microvolt = <2200000>;
			};

			ldo8: ldo8{
				regulator-name = "ldo8";
				regulator-min-microvolt  = <2100000>;
				regulator-max-microvolt = <3300000>;
			};

			ldo9: ldo9{
				regulator-name = "ldo9";
				regulator-min-microvolt  = <2600000>;
				regulator-max-microvolt = <3300000>;
			};
			};
		};
		atc2609a_adckeypad {
			#define KEY_VOLUMEDOWN	114
			#define KEY_VOLUMEUP	115
			compatible = "actions,atc2609a-adckeypad";
			keymapsize = <2>;
			adc_channel_name = "AUX0";
			key_val = <KEY_VOLUMEUP KEY_VOLUMEDOWN>;
			left_adc_val =  <0 3300>;
			right_adc_val = <100 3700>;
		};
		atc2609a-battery{
			compatible = "actions,atc2609a-battery";
			capacity = <7000>;/*unit:mAh*/
			shutdown_current = <50>;/*unit:ua*/
		};
		atc260x-charger{
			compatible = "actions,atc2609a-charger";
			rsense = <20>;/*unit:mohm*/
			support_adaptor_type = <3>; /*1: DCIN  2: USB  3:DCIN+USB*/
		};
	};

	backlight {
		compatible = "actions,s900-pwm-backlight";

		/* GPIOA14, active high */
		en-gpios = <&gpioa 14 0>;

		/*1.pwm num; 2. period in ns; */
		/*3.plarity, 0: high active, 1: low active*/
		pwms = <&pwm 2 50000 0>;

		total_steps = <1024>;
		min_brightness = <100>;
		max_brightness = <1000>;
		dft_brightness = <500>;

		delay_bf_pwm = <0>; /*in ms*/
		delay_af_pwm = <5>; /*in ms*/
	};

	hdmi@e0250000 {
		hdcp_onoff = <0>;
		channel_invert = <0>;
		bit_invert = <0>;

		panel@ghp {
			compatible = "actions,panel-ghp";

			is_primary = <0>;
		};
	};

	xhci@e0400000 {
		actions,vbus-gpio = <&gpiod 1 0>;  /* GPIOD1,  0:  high active ; 1: low active */
	};
};
