ifeq ($(TARGET_BOARD_PLATFORM), S700)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := libsub.so libbmp.a id_jpg.so libskia_actions_opt.a libactions_video_opt.a libdrm_algorithm.a resample.a
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := adAPE.so  adPCM.so  apAPE.so apWAV.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)



include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := avd_avi.so avd_flv.so  avd_mpg.so avd_ts.so \
vd_flv1.so vd_h263.so vd_h264.so  vd_mpeg.so vd_xvid.so vd_mjpg.so  \
libhde_core.so libvde_core.so\
libACT_ISP.so libACT_EncAPI.so libACT_VceResize.so libACT_FD.so libAutoFocus.so  libsw_isp.so \
libbacklight.so libimg_en.so libcrop_detect.so libhdri.so libhdcp.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := ALTestPlatform.apk
LOCAL_MODULE_TAGS := eng optional
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_PATH := $(TARGET_OUT_APPS)
LOCAL_SRC_FILES := $(LOCAL_MODULE)
LOCAL_CERTIFICATE := PRESIGNED
include $(BUILD_PREBUILT)


#video codec	

#LEVEL1 start
codec_PRODUCT_PACKAGES += \
	libsub \
	libbmp \
	id_jpg\
	adAPE \
	adPCM \
	apAPE \
	apWAV \
	avd_avi \
	avd_flv \
	avd_mpg \
	avd_ts \
	vd_flv1 \
	vd_h263 \
	vd_h264 \
	vd_mjpg \
	vd_mpeg \
	vd_xvid \
	libACT_ISP \
	libACT_EncAPI \
	libACT_VceResize \
	libACT_FD \
	libvde_core \
	libbacklight \
	libimg_en \
	libhde_core \
	libcrop_detect \
	libhdri \
	libhdcp
#LEVEL1 end	 

#LEVEL2 start
ifeq ($(words $(shell find $(LOCAL_PATH)/adDTS.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := adDTS.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	adDTS	
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/adFLAC.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := adFLAC.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	adFLAC	
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/adACELP.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := adACELP.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	adACELP	
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/adMP3.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := adMP3.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	adMP3
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/apDTS.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := apDTS.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	apDTS
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/apMP3.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := apMP3.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	apMP3
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/vd_vp6.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := vd_vp6.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	vd_vp6
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/vd_vp8.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := vd_vp8.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	vd_vp8
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/vd_vp9.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := vd_vp9.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	vd_vp9
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/vd_h265.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := vd_h265.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	vd_h265
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/vd_hevc_91.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := vd_hevc_91.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	vd_hevc_91
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/vd_avs.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := vd_avs.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	vd_avs
endif



#LEVEL2 end

#LEVEL3 start

ifeq ($(words $(shell find $(LOCAL_PATH)/adAC3.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := adAC3.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	adAC3
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/adCOOK.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := adCOOK.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	adCOOK
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/adWMALSL.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := adWMALSL.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	adWMALSL
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/adWMAPRO.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := adWMAPRO.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	adWMAPRO
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/adWMASTD.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := adWMASTD.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	adWMASTD
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/apAC3.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := apAC3.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	apAC3
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/apRMA.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := apRMA.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	apRMA
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/apWMA.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := apWMA.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	apWMA
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/avd_rm.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := avd_rm.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	avd_rm
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/avd_wmv.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := avd_wmv.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	avd_wmv
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/avd_ogm.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := avd_ogm.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	avd_ogm
endif


ifeq ($(words $(shell find $(LOCAL_PATH)/vd_msm4.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := vd_msm4.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	vd_msm4
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/vd_rv34.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := vd_rv34.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	vd_rv34
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/vd_vc1.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := vd_vc1.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	vd_vc1
endif

ifeq ($(words $(shell find $(LOCAL_PATH)/vd_wmv2.so)),1)
include $(CLEAR_VARS)
LOCAL_PREBUILT_LIBS := vd_wmv2.so
LOCAL_MODULE_TAGS := eng optional
include $(BUILD_MULTI_PREBUILT)
codec_PRODUCT_PACKAGES += \
	vd_wmv2
endif

#LEVEL3 end

ifneq ($(PRODUCT_PREBUILT_CAMERA), yes)
	codec_PRODUCT_PACKAGES += libjni_mosaic
endif

include $(CLEAR_VARS)
LOCAL_MODULE := libcodec_prebuilt
LOCAL_MODULE_TAGS := eng optional
LOCAL_REQUIRED_MODULES := $(codec_PRODUCT_PACKAGES)
include $(BUILD_PHONY_PACKAGE)

endif

