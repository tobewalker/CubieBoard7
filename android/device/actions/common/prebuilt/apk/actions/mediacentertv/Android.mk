
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := MediaCenterTv
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES   := MediaCenterTv.apk
LOCAL_OVERRIDES_PACKAGES := OWLPlayer
LOCAL_BUILT_MODULE_STEM := $(LOCAL_SRC_FILES)
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_TARGET_ARCH := arm
LOCAL_PREBUILT_JNI_LIBS := \
  @lib/armeabi-v7a/libavcodec-1.1.git.so \
  @lib/armeabi-v7a/libavformat-1.1.git.so \
  @lib/armeabi-v7a/libavutil-1.1.git.so \
  @lib/armeabi-v7a/libbluray_act.so \
  @lib/armeabi-v7a/libbluray_jni.so \
  @lib/armeabi-v7a/libbluray_owl.so \
  @lib/armeabi-v7a/libbluray_pgs_subtitle.so \
  @lib/armeabi-v7a/libowlplayer_jni.so \
  @lib/armeabi-v7a/libsubtitle.so \
  @lib/armeabi-v7a/libsubtitle_jni.so \
  @lib/armeabi-v7a/libvinit.so
  
include $(BUILD_PREBUILT)
