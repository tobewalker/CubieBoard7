#ifndef PROTECT_SW_API_
#define PROTECT_SW_API_

/*软件算法库保护，合法返回0， 非法返回<0*/
extern int acts_software_piracy_protection();

/*检查当前软件库版本信息,sizeof(s) = 8*/
extern void acts_software_get_version(unsigned char *s);
#endif