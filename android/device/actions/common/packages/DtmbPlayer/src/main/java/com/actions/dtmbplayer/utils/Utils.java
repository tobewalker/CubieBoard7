package com.actions.dtmbplayer.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.storage.StorageManager;
import android.util.Log;

import com.actions.dtmbplayer.service.RefreshMediaService;

public class Utils {
	private static final String TAG = Utils.class.getSimpleName();

	private static String[] getVolumePaths(StorageManager manager) {
		String[] result = null;
		result = (String[]) invokeMethodWithoutArgument(manager, "getVolumePaths");
		return result;
	}

	private static String getVolumeState(StorageManager manager, String storagePath) {
		Method methodId = null;
		String result = null;
		try {
			methodId = manager.getClass().getMethod("getVolumeState", String.class);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		if (methodId != null) {
			try {
				result = (String) methodId.invoke(manager, storagePath);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	private static Object invokeMethodWithoutArgument(Object object, String methodName) {
		Method methodId = null;
		Object result = null;
		try {
			methodId = object.getClass().getMethod(methodName);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		if (methodId != null) {
			try {
				result = methodId.invoke(object);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public static List<String> getAvailableStoragePaths(Context context) {
		List<String> result = new ArrayList<String>();
		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
		String[] volumePaths = getVolumePaths(storageManager);
		if (volumePaths != null) {
			for (String path : volumePaths) {
				String state = getVolumeState(storageManager, path);
				if (Environment.MEDIA_MOUNTED.equals(state)) {
					if (path.equals(externalStorage) || path.contains("sd") || path.contains("host")) {
						result.add(path);
					}
				}
			}
		}
		return result;
	}

	public static boolean isDatabaseExists(Context context, String dbName) {
		SQLiteDatabase db = null;
		try {
			db = SQLiteDatabase.openDatabase(context.getDatabasePath(dbName).getAbsolutePath(), null, SQLiteDatabase.OPEN_READONLY);
		} catch (Exception e) {
		}
		if (db != null) {
			db.close();
		}
		return db == null ? false : true;
	}

	public static final String PREFERENCES_FILE = "file_info";
	public static final String PREFERENCES_KEY_DATABASE_INIT = "database_init";

	public static boolean isDatabaseInit(Context context) {
		boolean isInit = false;
		SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, 0);
		isInit = preferences.getBoolean(PREFERENCES_KEY_DATABASE_INIT, false);
		return isInit;
	}

	public static void setDatabaseInit(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_FILE, 0);
		Editor editor = preferences.edit();
		editor.putBoolean(PREFERENCES_KEY_DATABASE_INIT, true).commit();
		editor.clear();
	}

	public static void copyFile(File sourceFile, File targetFile) throws IOException {
		BufferedInputStream inBuff = null;
		BufferedOutputStream outBuff = null;
		try {
			inBuff = new BufferedInputStream(new FileInputStream(sourceFile));
			outBuff = new BufferedOutputStream(new FileOutputStream(targetFile));
			byte[] b = new byte[1024];
			int len;
			while ((len = inBuff.read(b)) != -1) {
				outBuff.write(b, 0, len);
			}
			outBuff.flush();
		} finally {
			// 关闭
			if (inBuff != null)
				inBuff.close();
			if (outBuff != null)
				outBuff.close();
		}
	}

	public static File getCacheDir(Context context) {
		File cacheDir = null;
		String packageName = context.getPackageName();
		File externalStorageDirectory = Environment.getExternalStorageDirectory();
		if (!externalStorageDirectory.canWrite()) {
			String[] paths;
			String extSdCard = null;
			try {
				StorageManager sm = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
				paths = (String[]) sm.getClass().getMethod("getVolumePaths", new Class[0]).invoke(sm, new Object[0]);
				String esd = Environment.getExternalStorageDirectory().getPath();
				for (int i = 0; i < paths.length; i++) {
					if (paths[i].equals(esd)) {
						continue;
					}
					File sdFile = new File(paths[i]);
					if (sdFile.canWrite()) {
						extSdCard = paths[i];
						Log.i(TAG, "extsdcard:" + extSdCard);
					}
				}
				cacheDir = new File(extSdCard + "/" + packageName);
			} catch (Exception e) {
				e.printStackTrace();
				cacheDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + packageName);
			}
		} else {
			cacheDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + packageName);
		}

		if (cacheDir != null && !cacheDir.isDirectory()) {
			if (cacheDir.mkdir()) {
				RefreshMediaService.notifyFileChange(context, cacheDir.getAbsolutePath(), RefreshMediaService.MODE_ADD);
			}
		}
		return cacheDir;
	}
}
