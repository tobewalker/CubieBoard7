#ifndef __DTMB_H___
#define __DTMB_H___

#include "utils/Log.h"

#include <binder/ProcessState.h>

#include <media/IMediaHTTPService.h>
#include <media/IStreamSource.h>
#include <media/mediaplayer.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/DataSource.h>
#include <media/stagefright/MPEG2TSWriter.h>
#include <media/stagefright/MediaExtractor.h>
#include <media/stagefright/MediaSource.h>
#include <media/stagefright/MetaData.h>

#include <binder/IServiceManager.h>
#include <media/IMediaPlayerService.h>
#include <gui/ISurfaceComposer.h>
#include <gui/SurfaceComposerClient.h>
#include <gui/Surface.h>

#include <utils/Mutex.h>
//#include <utils/List.h>

#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <ui/DisplayInfo.h>
extern "C"{
	#include "parser.h"
}

using namespace android;

//todo:
//1. 修改填充buffer的方式。现在是用完之后再一次性读取满。考虑改为起一个线程，用一点读取一点。
//2. 回调函数异步工作方式。接收回掉函数，马上返回，另起线程queueBuffer。

class DtmbController {
public:
	// Object assumes ownership of fd.
	DtmbController();

	int getChannelInfo(channels_info *cInfo);

	//return val:
	//0: failed;
	//1: success, freq changed
	//2: success, freq doesn't change
	int setDTMBFreq(unsigned freq);
	void clearDTMBFreq();
	void setChannelNum(unsigned channelNum);
	int getBER();
	bool isLocked();

	const static int kTsPacketLen = 188;
	const static int kDtmbBufferSize = kTsPacketLen * 1024;// 1024 packet
	bool isIdle() const { return mStatus == Dtmb_Idle; }
	void init();
	void release();
	void dumpStatus();

	void clearSource();
	sp<IStreamSource> getSource();
	
	virtual ~DtmbController();

private:
	typedef enum{
		Dtmb_Error = -1,
		Dtmb_Idle,
		Dtmb_Loaded,
		Dtmb_Locked,
		Dtmb_Running
	} DTMB_STATUS;

	class DtmbStreamSource : public BnStreamSource {
	public:
		DtmbStreamSource(DtmbController *owner);
		~DtmbStreamSource();
		virtual void setListener(const sp<IStreamListener> &listener);
		virtual void setBuffers(const Vector<sp<IMemory> > &buffers);

		virtual void onBufferAvailable(size_t index);

	private:
		sp<IStreamListener> mListener;
		Vector<sp<IMemory> > mBuffers;

		DtmbController *mOwner;

		DISALLOW_EVIL_CONSTRUCTORS(DtmbStreamSource);
	};

	int mFd;
	unsigned int mFreq;
	unsigned mChannelNum;
	uint64_t mNumPacketsSent;
	bool mHeadConfirm;
	DTMB_STATUS mStatus;
	Mutex mLock;

	dtmb_buf in_buf;

	sp<DtmbStreamSource> mSource;

	//must sure that packet buf len > 5 * packetLen
	int packetHeadPosition(int packetLen);

	int fullInbufIfNeed(dtmb_buf *inBuf);
	void dumpChannelInfo(channels_info *cInfo);
	void allocInbuf();
	void flushInbuf();
	bool dtmbCaptureOn();
	bool dtmbCaptureOff();
	
	DISALLOW_EVIL_CONSTRUCTORS(DtmbController);
};

#endif