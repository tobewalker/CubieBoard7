package com.actions.dtmbplayer.widget;

import java.util.Observable;
import java.util.Observer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actions.dtmbplayer.R;
import com.actions.dtmbplayer.data.ProgramList;

public class ProgramListAdapter extends BaseRecyclerViewAdapter<ProgramListAdapter.ViewHolder> implements Observer {
	private static final String TAG = ProgramListAdapter.class.getSimpleName();
	private ProgramList mProgramList;
	private int mSelection = -1;

	public ProgramListAdapter(ProgramList list) {
		mProgramList = list;
		mProgramList.addObserver(this);
	}

	@Override
	public int getItemCount() {
		return mProgramList.size();
	}

	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int position) {
		super.onBindViewHolder(viewHolder, position);
		viewHolder.itemView.setActivated(position == mSelection);
		viewHolder.textViewProgramName.setText(mProgramList.get(position).programName);
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		View rootView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.program_item_layout, viewGroup, false);
		return new ViewHolder(rootView);
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public TextView textViewProgramName;

		public ViewHolder(View rootView) {
			super(rootView);
			textViewProgramName = (TextView) rootView.findViewById(R.id.program_name);
		}

	}

	@Override
	public void update(Observable observable, Object data) {
		notifyDataSetChanged();
	}

	public void setSelection(int selection) {
		mSelection = selection;
	}
	
	public void clearSelection() {
		mSelection = -1;
	}
}
