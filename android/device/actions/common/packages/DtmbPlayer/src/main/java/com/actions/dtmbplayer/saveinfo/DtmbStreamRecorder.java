package com.actions.dtmbplayer.saveinfo;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import android.content.Context;

import com.actions.dtmbplayer.service.RefreshMediaService;
import com.actions.dtmbplayer.utils.Utils;
import com.actions.dtmbplayer.widget.DtmbPlayer;

public class DtmbStreamRecorder {
	private DtmbPlayer mDtmbPlayer;
	private Context mContext;
	private String mFilePath;

	public DtmbStreamRecorder(Context context) {
		mContext = context;
		mDtmbPlayer = DtmbPlayer.getInstance(mContext);
	}

	public void StartRecord() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String date = simpleDateFormat.format(System.currentTimeMillis());
		String freq = "tmp";
		if (mDtmbPlayer.getChannel() != null) {
			freq = Integer.toString(mDtmbPlayer.getChannel().frequency);
		}
		String fileName = freq + "HZ_" + date + ".ts";

		File cacheDir = Utils.getCacheDir(mContext);
		if (cacheDir != null && cacheDir.exists() && cacheDir.canRead()) {
			File streamFile = new File(cacheDir, fileName);
			if (!streamFile.exists()) {
				try {
					streamFile.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (streamFile.exists() && streamFile.canWrite()) {
				mFilePath = streamFile.getAbsolutePath();
				mDtmbPlayer.setDebugStreamFilePath(streamFile.getAbsolutePath());
				mDtmbPlayer.startDebugRecord();
			}
		}
	}

	public void stopRecord() {
		mDtmbPlayer.stopDebugRecord();
		if (mFilePath != null) {
			RefreshMediaService.notifyFileChange(mContext, mFilePath, RefreshMediaService.MODE_ADD);
		}
	}
}
