package com.actions.settings.widget;

import com.actions.settings.R;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.VolumePreference.VolumeStore;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;

public class VolumePreference extends Preference implements OnClickListener,
		SeekBarVolumizer.Callback,
		View.OnKeyListener {

	private static final String TAG = "VolumePreference";
	private Context mContext;
	private ImageButton decreaseButton;
	private ImageButton increaseButton;
	private SeekBar volumeBar;
	private SeekBarVolumizer mSeekBarVolumizer;
	private int mStreamType;
	private Uri mDefaultUri;

	
	public VolumePreference(Context context) {
		super(context, null);
		mContext = context;
		// TODO Auto-generated constructor stub
		
	}
	

	public VolumePreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
		
	}

	public VolumePreference(Context context, AttributeSet arg1, int arg2) {
		super(context, arg1);
		// TODO Auto-generated constructor stub
		mContext = context;
		
	}

	  private Uri getMediaVolumeUri(Context context) {
	        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
	                + context.getPackageName()
	                + "/" + R.raw.media_volume);
	    }
	@Override
	protected View onCreateView(ViewGroup parent) {
		// // TODO Auto-generated method stub
		Log.v(TAG, "onCreateView");
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View layout = inflater.inflate(R.layout.volume_prefence, null);
		mStreamType = AudioManager.STREAM_MUSIC;
		mDefaultUri =getMediaVolumeUri(mContext);
		
		
		return layout;
		// return super.onCreateView(parent);
	}

	@Override
	protected void onBindView(View view) {
		// TODO Auto-generated method stub
		//Log.e(TAG, "onBindView");
		super.onBindView(view);
		decreaseButton = (ImageButton) view.findViewById(R.id.volumeDecrease);
		increaseButton = (ImageButton) view.findViewById(R.id.volumeIncrease);
		decreaseButton.setOnClickListener(this);
		increaseButton.setOnClickListener(this);
		volumeBar = (SeekBar) view.findViewById(R.id.volumeBar);
		initLinstener();
		view.setOnKeyListener(this);
	}


	public void initLinstener(){
		if (mSeekBarVolumizer == null){
				mSeekBarVolumizer = new SeekBarVolumizer(mContext, mStreamType,
					mDefaultUri, this);
		}
		if(volumeBar != null){
			mSeekBarVolumizer.setSeekBar(volumeBar);
		}
	}
	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.volumeDecrease:
			Log.e(TAG, "volume --");
			mSeekBarVolumizer.changeVolumeBy(-1);

			break;
		case R.id.volumeIncrease:
			Log.e(TAG, "volume ++");
			mSeekBarVolumizer.changeVolumeBy(1);
			break;
		}
	}
	public void changeVolume(int value){
		mSeekBarVolumizer.changeVolumeBy(value);
	}
	@Override
	public void onSampleStarting(SeekBarVolumizer sbv) {
		// TODO Auto-generated method stub
		 if (mSeekBarVolumizer != null && sbv != mSeekBarVolumizer) {
	            mSeekBarVolumizer.stopSample();
	        }
	}

	public void stopSample(){
		if (mSeekBarVolumizer != null) {
            mSeekBarVolumizer.stopSample();
        }
	}
	public void cleanup() {
		if (mSeekBarVolumizer != null) {
			volumeBar.setOnKeyListener(null);
			mSeekBarVolumizer.stop();
			mSeekBarVolumizer = null;
		}
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state == null || !state.getClass().equals(SavedState.class)) {
			// Didn't save state for us in onSaveInstanceState
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState myState = (SavedState) state;
		super.onRestoreInstanceState(myState.getSuperState());
		if (mSeekBarVolumizer != null) {
			mSeekBarVolumizer.onRestoreInstanceState(myState.getVolumeStore());
		}
	}

	@Override
	protected Parcelable onSaveInstanceState() {
		final Parcelable superState = super.onSaveInstanceState();
		if (isPersistent()) {
			// No need to save instance state since it's persistent
			return superState;
		}

		final SavedState myState = new SavedState(superState);
		if (mSeekBarVolumizer != null) {
			mSeekBarVolumizer.onSaveInstanceState(myState.getVolumeStore());
		}
		return myState;
	}

	private static class SavedState extends BaseSavedState {
		VolumeStore mVolumeStore = new VolumeStore();

		public SavedState(Parcel source) {
			super(source);
			mVolumeStore.volume = source.readInt();
			mVolumeStore.originalVolume = source.readInt();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(mVolumeStore.volume);
			dest.writeInt(mVolumeStore.originalVolume);
		}

		VolumeStore getVolumeStore() {
			return mVolumeStore;
		}

		public SavedState(Parcelable superState) {
			super(superState);
		}

		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}

}
