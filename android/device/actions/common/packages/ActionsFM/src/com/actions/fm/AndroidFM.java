package com.actions.fm;


public class AndroidFM {
	static{
		System.loadLibrary("actionsfmjni");
	}
	public native static int initFM();
	public native static int releaseFm();
	public native static int autoSeek(int direction);
	public native static int getFrequency();
	public native static int setFrequency(int freq);
	public native static int getVolume();
	public native static int setVolume(int volume);
	public native static int setMute(int state);
}
