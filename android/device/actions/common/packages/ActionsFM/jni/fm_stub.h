#ifndef __FM_STUB_H__
#define __FM_STUB_H__
int fm_init(void);
int fm_deinit(void);
int hw_seek(int direction);
int get_frequency(void);
int set_frequency(int freq);
int get_volume(void);
int set_volume(int volume);
int set_mute(int state);

#endif