package com.actions.miracast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pWfdInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.view.Surface;
import android.widget.Toast;

import com.actions.miracast.util.WFDDeviceCapability;
import com.actions.miracast.util.WFDNative;
import com.actions.wfdinterface.IWFDAVManager;
import com.actions.wfdinterface.IWFDSessionClient;
import com.actions.wfdinterface.IWFDSessionClientListener;

public class WFDSinkService extends Service {

	public static String TAG = "WFDSinkService";

	private int timeoutMs = 10000;
	private NotificationManager mNM;
	private IWFDAVManager mWFDAVManager = null;
	private IWFDSessionClient mWFDSessionClient = null;
	private WifiP2pWfdInfo mWfdInfo = null;
	//modify by xusongzhou
	public Binder mBinder = new MyBinder();

	private Channel mChannel = null;
	private WifiP2pManager mWifiP2pManager = null;

	private BroadcastReceiver receiver = null;
	private final IntentFilter intentFilter = new IntentFilter();

	SharedPreferences mPreferences;

	private String serverIP;
	private int serverPort;
    private int wfdServicePid;

	public WFDSinkStateMachine mWFDSinkStateMachine = null;

	public static final String WIFI_DISPLAY_SINK_WPS_MESSAGE = "com.hoperun.wifidisply.WPS_MESSAGE";

	public static final String WIFI_DISPLAY_RTSP_PORT_MESSAGE = "com.android.wifidisply.RTSP_PORT_MESSAGE";
	private final static String WFD_PATH = "/data/data/com.hoperun.wifidisplay/wfd/";
	private final static String WFD_FILE_NAME = "wfdservice";
	private final static String WFD_FILE_FULLNAME = WFD_PATH + WFD_FILE_NAME;
	private final static String WFD_JNI_NAME = "libwfd_jni.so";
	private final static String WFD_JNI_FULLNAME = "/data/data/com.hoperun.wifidisplay/lib/" + WFD_JNI_NAME;
	private static final int NOTIFICATION_STATE = 1;



      public WFDSinkActivity activity =null;








	public boolean isWFDAVManagerConnected() {
		return mWFDAVManager == null ? false : true;
	}

	private IWFDSessionClientListener.Stub mWFDSessionClientListener = new IWFDSessionClientListener.Stub() {
		@Override
		public IBinder asBinder() {
			return mWFDSessionClientListener;
		}

		@Override
		public void onWFDSessionClientCreated(IWFDSessionClient sessClient)
				throws RemoteException {
			Log.i(TAG, "WFD session is created.");
			mWFDSinkStateMachine
					.sendMessage(WFDSinkStateMachine.SESSION_CLIENT_CREATED);
			mWFDSessionClient = sessClient;
		}

		@Override
		public void notify(int code, int arg0, int arg1) throws RemoteException {
		}

		@Override
		public void onWFDSessionClientDestroyed() throws RemoteException {
			Log.i(TAG, "WFD session is destroyed.");
			destroyP2pConnection();
			mWFDSinkStateMachine
					.sendMessage(WFDSinkStateMachine.SESSION_CLIENT_DESTROYED);
			//mWFDSessionClient = null;
		}
	};

	public class MyBinder extends Binder {
		WFDSinkService getService() {
			return WFDSinkService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

    private Boolean wfdServiceFileCheckExistAndCreate() {
        File dir = new File(WFD_PATH);
        if(!dir.exists()) {
        	Log.i(TAG, "wfd dirtexist now create");
        	dir.mkdir();
        }
        
        dir = new File("/data/data/com.hoperun.wifidisplay/lib/");
        if(!dir.exists()) {
        	Log.i(TAG, "lib dirtexist now create");
        	dir.mkdir();
        }
        
        File file = new File(WFD_FILE_FULLNAME);
        if (!file.exists())
        {
            try
            {
            	Log.i(TAG, "wfd file not exist now create");
                //Create files in the specified folder
                file.createNewFile();
                file.setExecutable(true);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
        }
        
        file = new File(WFD_JNI_FULLNAME);
        if (!file.exists())
        {
            try
            {
            	Log.i(TAG, "lib jni file not exist now create");
                // Create files in the specified folder
                file.createNewFile();
                file.setExecutable(true);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
        } else {
            return true;
        }
        
        return false;
    }

    private int copyFileFromAssert(String assertName, String destFilename) throws IOException
    {
        InputStream myInput;
        OutputStream myOutput = new FileOutputStream(destFilename);
        Log.i(TAG, "copyFileFromAssert");
        myInput = this.getAssets().open(assertName);
        byte[] buffer = new byte[1024];
        int length;
        Log.i(TAG, "start copy");
        while ((length = myInput.read(buffer)) > 0)
        {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myInput.close();
        myOutput.close();
        
        return 0;
    }

    private int startWFDService() {

        if(wfdServiceFileCheckExistAndCreate()) {
        	Log.i(TAG, "wfdservice already exist");
        	return 0;
        }
        
        try
        {
        	copyFileFromAssert(WFD_FILE_NAME, WFD_FILE_FULLNAME);
        	copyFileFromAssert(WFD_JNI_NAME, WFD_JNI_FULLNAME);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        int retval = WFDNative.wfdLoader(WFD_FILE_FULLNAME);
        
        if(retval < 0) {
            Log.e(TAG, "startWFDService fail!");
        } else {
            wfdServicePid = retval;
        }
        
        return -1;
    }
    
    private int killWFDService() {
     
        return 0;
    }

	@Override
	public void onCreate() {
		super.onCreate();
		//startWFDService();
		// Get the notification service to show tips on status bar
		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Get the WFDAVManager service
		IBinder binder;
		if (android.os.SystemProperties.get("persist.wifidisplay.avmanager",
				"real").equals("fake")) {
			Log.i(TAG, "Use fake avmanager");
			binder = ServiceManager
					.getService(WFDDeviceCapability.FAKE_AVMANAGER_SERVICE_NAME);
		} else {
			Log.i(TAG, "Use real avmanager");
			binder = ServiceManager
					.getService(WFDDeviceCapability.REAL_AVMANAGER_SERVICE_NAME);
		}
		mWFDAVManager = IWFDAVManager.Stub.asInterface(binder);
		if (mWFDAVManager == null) {
			Log.e(TAG, "Connect to WFDAVManager failed listservice");
			try {
				int i;
				String[] sl = ServiceManager.listServices();
				for(i = 0; i < sl.length; i++) {
					Log.e(TAG, i + sl[i]);
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		} else {
			Log.i(TAG, "Connect to WFDAVManager succeed.");
		}
		// Get the WiFi P2p service
		mWifiP2pManager = (WifiP2pManager)getSystemService(Context.WIFI_P2P_SERVICE);
		mChannel = mWifiP2pManager.initialize(this, getMainLooper(), null);
		// Start the state machine
		initStateMachine();
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
		intentFilter.addAction(WFDSinkService.WIFI_DISPLAY_SINK_WPS_MESSAGE);
		intentFilter.addAction(WFDSinkService.WIFI_DISPLAY_RTSP_PORT_MESSAGE);

		receiver = new WFDSinkServiceReceiver(mWifiP2pManager, mChannel,
				mWFDSinkStateMachine, this);
		registerReceiver(receiver, intentFilter);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		Log.i(TAG, "onDestroy WFDSinkService");
		mNM.cancelAll();
		if (receiver != null) {
			unregisterReceiver(receiver);
		}
		super.onDestroy();
	}

	public void showNotification(int tipID) {
		CharSequence text = getText(tipID);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				new Intent(this, WFDSinkActivity.class), 0);
		Builder nb = new Notification.Builder(this);
		nb.setDefaults(Notification.DEFAULT_VIBRATE
				| Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND);
		nb.setContentIntent(contentIntent);
		nb.setContentTitle(getText(R.string.wfd_sink_service_label));
		nb.setContentText(text);
		nb.setSmallIcon(R.drawable.sink_notification);
		nb.setTicker(text);

		mNM.notify(NOTIFICATION_STATE, nb.getNotification());
	}

	public WifiP2pWfdInfo getWFDDeviceInfo() {
		try {
			mWfdInfo = new WifiP2pWfdInfo(WFDDeviceCapability.wfd_device_pri_snk
					| (WFDDeviceCapability.wfd_available_wfd_session << 4),
					mWFDAVManager.getRTSPPort(),
					50);
			Log.i(TAG, "Register WFD info.... "+ mWfdInfo);
			return mWfdInfo;
		} catch (RemoteException exception) {
			Log.e(TAG, exception.toString());
			return null;
		}
	}

	public void startClient(Surface surface) {
		try {
			Log.e(TAG, "Start WFD Session Client " + serverIP + ":" + serverPort
					+ " timeoutMs=" + timeoutMs + " listener="
					+ mWFDSessionClientListener.toString() + " surface="
					+ surface.toString());
			// In case of mediaServer restarts
			if (mWFDAVManager == null
					|| !mWFDAVManager.asBinder().isBinderAlive()) {
				IBinder binder;
				if (android.os.SystemProperties.get(
						"persist.wifidisplay.avmanager", "real").equals("fake")) {
					Log.i(TAG, "Use fake avmanager");
					binder = ServiceManager
							.getService(WFDDeviceCapability.FAKE_AVMANAGER_SERVICE_NAME);
				} else {
					Log.i(TAG, "Use real avmanager");
					binder = ServiceManager
							.getService(WFDDeviceCapability.REAL_AVMANAGER_SERVICE_NAME);
				}
				mWFDAVManager = IWFDAVManager.Stub.asInterface(binder);
			}
			mWFDAVManager.startWFDSessionClient(serverIP, serverPort,
					timeoutMs, mWFDSessionClientListener, surface);
		    Log.e(TAG, "startWFDSessionClient ok");
		} catch (RemoteException exception) {
			Log.e(TAG, exception.toString());
		}
	}

	public void teardownClient() {		        								
		try {
			Log.i(TAG, "Teardown WFD Session Client");
			if (mWFDSessionClient != null
					&& mWFDSessionClient.asBinder().isBinderAlive()) {
				mWFDSessionClient.teardown();
			}
		} catch (RemoteException exception) {
			Log.e(TAG, exception.toString());
		}
		mWFDSessionClient = null;		
	}

	public void resetAVManager() {
		// try {
		Log.i(TAG, "Reset WFD AVManager Client");
		if (mWFDAVManager != null && mWFDAVManager.asBinder().isBinderAlive()) {
			// mWFDAVManager.reset();
		}
		mWFDSessionClient = null;
	}

	public void setServerInfo(int wfdRTPPort, InetAddress wfdRTPServerIP) {
		serverPort = wfdRTPPort;
		serverIP = wfdRTPServerIP.getHostAddress();
		Log.i(TAG, "serverPort:"+ serverPort + "    serverIP:" + serverIP);
	}

	public void initStateMachine() {
		mWFDSinkStateMachine = new WFDSinkStateMachine(this);
		mWFDSinkStateMachine.start();
	}

	public WFDSinkStateMachine getWFDSinkStateMachine() {
		return mWFDSinkStateMachine;
	}

	public void registerWFDDeviceInfo() {
		if (isWFDAVManagerConnected() == false) {
			return;
		}

		mWifiP2pManager.setWFDInfo(mChannel, getWFDDeviceInfo(),
				new WifiP2pManager.ActionListener() {

					@Override
					public void onSuccess() {
						Log.i(TAG, "Register Device Info Succeed.");
						mWFDSinkStateMachine
								.sendMessage(
										WFDSinkStateMachine.REGISTER_DEVICE_INFO_RESULT,
										true);
					}

					@Override
					public void onFailure(int reasonCode) {
						Log.e(TAG, "Register Device Info Failed:" + reasonCode);
						mWFDSinkStateMachine
								.sendMessage(
										WFDSinkStateMachine.REGISTER_DEVICE_INFO_RESULT,
										false);
					}
				});
				
		mWFDSinkStateMachine.sendMessageDelayed(
	    		WFDSinkStateMachine.REGISTER_DEVICE_INFO_TIMEOUT,
				WFDSinkStateMachine.REGISTER_DEVICE_INFO_TIMEOUT_TIME);
	}

	public void resetWFDDeviceInfo(boolean available) {
		if (isWFDAVManagerConnected() == false) {
			return;
		}

		if (available == true) {
			Log.i(TAG, "Reset Session Info to available.");
			//mWfdInfo.wfdDeviceInformation = WFDDeviceCapability.wfd_device_pri_snk
			//		| (WFDDeviceCapability.wfd_available_wfd_session << 4);
		} else {
			Log.i(TAG, "Reset Session Info to unavailable.");
			//mWfdInfo.wfdDeviceInformation = WFDDeviceCapability.wfd_device_pri_snk
			//		| (WFDDeviceCapability.wfd_available_not_wfd_session << 4);
		}
/*
		mWifiP2pManager.setWFDInfo(mChannel, mWfdInfo,
				new WifiP2pManager.ActionListener() {
					@Override
					public void onSuccess() {
						Log.i(TAG, "Reset Session Info Succeed.");
					}

					@Override
					public void onFailure(int reasonCode) {
						Log.e(TAG, "Reset Session Info Failed:" + reasonCode);
					}
				}
	    );
*/
	}

	public void destroyP2pConnection() {
		mWifiP2pManager.removeGroup(mChannel, new ActionListener() {

			@Override
			public void onFailure(int reasonCode) {
//				Toast.makeText(WFDSinkService.this,
//						"Disconnect failed. Reason:" + reasonCode,
//						Toast.LENGTH_SHORT).show();
				Log.e(TAG, "Disconnect failed. Reason:" + reasonCode);
			}

			@Override
			public void onSuccess() {
//				Toast.makeText(WFDSinkService.this, "Disconnect success.",
//						Toast.LENGTH_SHORT).show();
				Log.d(TAG, "Disconnect success.");
			}
		});
	}

	public void setWifiP2pEnabled(boolean enabled) {
		if (enabled) {
			mWFDSinkStateMachine
					.sendMessage(WFDSinkStateMachine.USER_TURN_ON_WIFI_DIRECT);
		} else {
			mWFDSinkStateMachine
					.sendMessage(WFDSinkStateMachine.USER_TURN_OFF_WIFI_DIRECT);
		}
	}

	//add by wfdsink
	public void disableWFDsink(){
		WifiP2pWfdInfo wfdInfo = new WifiP2pWfdInfo();
        wfdInfo.setWfdEnabled(false);
        mWifiP2pManager.setWFDInfo(mChannel, wfdInfo, new ActionListener() {
        	@Override
        	public void onSuccess() {
            		Log.v(TAG, "Successfully disable WFD.");
        	}
        	@Override
            public void onFailure(int reason) {
                	Log.v(TAG, "Failed to disable WFD with reason " + reason + ".");
            }
        });
	}

	public int getRtspPort(){
		if(receiver != null)
			return ((WFDSinkServiceReceiver)receiver).getRtspPort();
		else
			return -1;
	}
	
}
