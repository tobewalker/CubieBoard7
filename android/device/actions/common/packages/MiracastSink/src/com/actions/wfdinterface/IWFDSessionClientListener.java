/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/wangzh/mk2-wfd/vendor/rh/generic/wifidisplay/WiFiDisplayApplication/src/com/rh/wfd/IWFDSessionClientListener.aidl
 */
package com.actions.wfdinterface;
public interface IWFDSessionClientListener extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.actions.wfdinterface.IWFDSessionClientListener
{
private static final java.lang.String DESCRIPTOR = "com.actions.wfdinterface.IWFDSessionClientListener";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.actions.wfdinterface.IWFDSessionClientListener interface,
 * generating a proxy if needed.
 */
public static com.actions.wfdinterface.IWFDSessionClientListener asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.actions.wfdinterface.IWFDSessionClientListener))) {
return ((com.actions.wfdinterface.IWFDSessionClientListener)iin);
}
return new com.actions.wfdinterface.IWFDSessionClientListener.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_onWFDSessionClientCreated:
{
data.enforceInterface(DESCRIPTOR);
com.actions.wfdinterface.IWFDSessionClient _arg0;
_arg0 = com.actions.wfdinterface.IWFDSessionClient.Stub.asInterface(data.readStrongBinder());
this.onWFDSessionClientCreated(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_notify:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
int _arg1;
_arg1 = data.readInt();
int _arg2;
_arg2 = data.readInt();
this.notify(_arg0, _arg1, _arg2);
reply.writeNoException();
return true;
}
case TRANSACTION_onWFDSessionClientDestroyed:
{
data.enforceInterface(DESCRIPTOR);
this.onWFDSessionClientDestroyed();
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.actions.wfdinterface.IWFDSessionClientListener
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
// when IWFDSessionClient is created, this callback will be called

public void onWFDSessionClientCreated(com.actions.wfdinterface.IWFDSessionClient sessClient) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((sessClient!=null))?(sessClient.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_onWFDSessionClientCreated, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
// notify

public void notify(int code, int arg0, int arg1) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(code);
_data.writeInt(arg0);
_data.writeInt(arg1);
mRemote.transact(Stub.TRANSACTION_notify, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
// when IWFDSessionClient is destroyed, this callback will be called

public void onWFDSessionClientDestroyed() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_onWFDSessionClientDestroyed, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_onWFDSessionClientCreated = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_notify = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_onWFDSessionClientDestroyed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
}
// when IWFDSessionClient is created, this callback will be called

public void onWFDSessionClientCreated(com.actions.wfdinterface.IWFDSessionClient sessClient) throws android.os.RemoteException;
// notify

public void notify(int code, int arg0, int arg1) throws android.os.RemoteException;
// when IWFDSessionClient is destroyed, this callback will be called

public void onWFDSessionClientDestroyed() throws android.os.RemoteException;
}
