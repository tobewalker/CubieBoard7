/**
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.actions.miracast.util;

import android.os.Message;
import android.util.Log;

/**
 * {@hide}
 *
 * The class for implementing states in a StateMachine
 */
public class WFDNative {
    public static String TAG = "WFDNative";
    public static final String WFDJNI_DHCP_ACTION = "wfdjni.dhcp.action";
    static {
        System.loadLibrary("wfd_jni");
    }
    public static native int startDhcpSearch(String MACStr);

    public static native int wfdLoader(String path);
       
    
    public static void setDhcpIp(String MACStr, String IPStr)
    {
        //Intent intent = new Intent(WFDJNI_DHCP_ACTION);
        
        //sendbroadcast();
        Log.i(TAG, "ADDR:" + MACStr + " " + IPStr);
    }
}
