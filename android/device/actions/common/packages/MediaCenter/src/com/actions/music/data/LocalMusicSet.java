package com.actions.music.data;

import java.util.ArrayList;
import java.util.List;

import com.actions.mediacenter.data.MediaObject;
import com.actions.mediacenter.data.MediaSet;
import com.actions.mediacenter.data.Path;

public class LocalMusicSet extends MediaSet {
	private List<LocalMusic> mMusicList;
	private Path mPath;

	public LocalMusicSet(Path path) {
		mMusicList = new ArrayList<LocalMusic>();
		mPath = path;
	}

	@Override
	public MediaObject getItem(int position) {
		if (position >= 0 && position < size()) {
			return mMusicList.get(position);
		}
		return null;
	}

	@Override
	public void addItem(MediaObject item) {
		mMusicList.add((LocalMusic) item);
	}

	@Override
	public int size() {
		return mMusicList.size();
	}

	@Override
	public String getPath() {
		return mPath.getPath();
	}

	@Override
	public String getName() {
		return mPath.getName();
	}

	@Override
	public void clear() {
		mMusicList.clear();
	}

}
