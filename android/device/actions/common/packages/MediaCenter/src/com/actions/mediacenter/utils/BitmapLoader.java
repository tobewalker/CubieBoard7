package com.actions.mediacenter.utils;

import java.lang.ref.SoftReference;
import java.util.HashMap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;

import com.actions.owlplayer.app.OWLApplication;
import com.actions.owlplayer.util.Future;
import com.actions.owlplayer.util.FutureListener;
import com.actions.owlplayer.util.ThreadPool;
import com.actions.owlplayer.util.ThreadPool.Job;
import com.actions.owlplayer.util.ThreadPool.JobContext;

public class BitmapLoader {
	private static final String TAG = "BitmapLoader";

	public static final int RWQUEST_DECODE_BOUND = 0;
	public static final int REQUEST_THUMB_MINI = 1;
	public static final int REQUEST_THUMB_FULL = 2;

	private static final int THUMBNAIL_WIDTH = 256;
	private static final int THUMBNAIL_HEIGHT = 256;
	private static final int MAX_WIDTH = 1024;
	private static final int MAX_HEIGHT = 1024;

	private static ThreadPool mThreadPool;
	private static BitmapLoader mBitmapLoader = null;
	private static HashMap<Integer, SoftReference<Bitmap>> mMiniImageCache = null;
	private static HashMap<Integer, SoftReference<Bitmap>> mFullImageCache = null;

	public static BitmapLoader getInstance(OWLApplication app) {
		if (mBitmapLoader == null) {
			mBitmapLoader = new BitmapLoader();
		}

		if (mMiniImageCache == null) {
			mMiniImageCache = new HashMap<Integer, SoftReference<Bitmap>>();
		}

		if (mFullImageCache == null) {
			mFullImageCache = new HashMap<Integer, SoftReference<Bitmap>>();
		}

		mThreadPool = app.getThreadPool();
		return mBitmapLoader;
	}

	public Bitmap loadBitmap(final String path, final int requestType, final ImageCallback imageCallback) {
		SoftReference<Bitmap> reference = null;
		if (requestType == REQUEST_THUMB_MINI) {
			reference = mMiniImageCache.get(path.hashCode());
		} else if (requestType == REQUEST_THUMB_FULL) {
			reference = mFullImageCache.get(path.hashCode());
		}

		if (reference != null) {
			Bitmap bitmap = reference.get();
			if (bitmap != null) {
				return bitmap;
			}
		}

		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				imageCallback.imageLoad(path, (Bitmap) msg.obj);
			}
		};

		mThreadPool.submit(new LocalImageRequest(path, requestType), new FutureListener<Bitmap>() {
			@Override
			public void onFutureDone(Future<Bitmap> future) {
				Bitmap bitmap = future.get();
				if (requestType == REQUEST_THUMB_MINI) {
					mMiniImageCache.put(path.hashCode(), new SoftReference<Bitmap>(bitmap));
				} else if (requestType == REQUEST_THUMB_FULL) {
					mFullImageCache.put(path.hashCode(), new SoftReference<Bitmap>(bitmap));
				}

				Message msg = handler.obtainMessage(0, bitmap);
				handler.sendMessage(msg);
			}
		});
		return null;
	}

	public interface ImageCallback {
		public void imageLoad(final String viewTag, final Bitmap bitmap);
	}

	public static class LocalImageRequest implements Job<Bitmap> {
		private String mLocalFilePath;
		private int mRequestType;

		LocalImageRequest(String path, int requestType) {
			mLocalFilePath = path;
			mRequestType = requestType;
		}

		private int getSampleSize(int width, int height) {
			int wSampleSize = 0;
			int hSampleSize = 0;
			int requestWidth = 0;
			int requestHeight = 0;

			if (mRequestType == REQUEST_THUMB_MINI) {
				requestWidth = THUMBNAIL_WIDTH;
				requestHeight = THUMBNAIL_HEIGHT;
			} else {
				requestWidth = MAX_WIDTH;
				requestHeight = MAX_HEIGHT;
			}

			if (width > requestWidth) {
				wSampleSize = width / requestWidth;
			}

			if (height > requestHeight) {
				hSampleSize = height / requestHeight;
			}
			return wSampleSize > hSampleSize ? wSampleSize : hSampleSize;
		}

		@Override
		public Bitmap run(JobContext jc) {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(mLocalFilePath, options);
			if (mRequestType == RWQUEST_DECODE_BOUND) {
				Bitmap bitmap = Bitmap.createBitmap(options.outWidth, options.outHeight, Bitmap.Config.ARGB_8888);
				return bitmap;
			}
			options.inSampleSize = getSampleSize(options.outWidth, options.outHeight);
			options.inJustDecodeBounds = false;
			return BitmapFactory.decodeFile(mLocalFilePath, options);
		}
	}
}
