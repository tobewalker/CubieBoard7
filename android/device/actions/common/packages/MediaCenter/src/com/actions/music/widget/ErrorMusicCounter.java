package com.actions.music.widget;


public class ErrorMusicCounter {
	private static final int THRESHOLD = 3;
	private int count = 0;

	public void increase() {
		count++;
	}

	public void clear() {
		count = 0;
	}

	public boolean isLimitExceeded() {
		return count >= THRESHOLD;
	}
}
