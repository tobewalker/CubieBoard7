package com.actions.owlplayer.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

import com.actions.mediacenter.R;

public class ImageButtonWithState extends ImageButton {
	public final static int STATE_UNKNOWN = 0;
	public final static int STATE_PLAY = 1;
	public final static int STATE_PAUSE = 2;

	private int mState = STATE_UNKNOWN;

	private static final int[] PLAY_STATE_SET = { R.attr.state_play };

	private static final int[] PAUSE_STATE_SET = { R.attr.state_pause };

	public ImageButtonWithState(Context context) {
		this(context, null);
	}

	public ImageButtonWithState(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ImageButtonWithState(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void setState(int state) {
		mState = state;
		refreshDrawableState();
	}

	@Override
	public int[] onCreateDrawableState(int extraSpace) {
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
		int[] additionalState = {};

		if (mState == STATE_PLAY) {
			additionalState = PLAY_STATE_SET;
		}

		if (mState == STATE_PAUSE) {
			additionalState = PAUSE_STATE_SET;
		}

		mergeDrawableStates(drawableState, additionalState);

		return drawableState;
	}
}
