package com.actions.mediacenter.widget;

import android.widget.TextView;

import com.actions.mediacenter.R;
import com.actions.mediacenter.data.ActionMode;

public class SelectedItemShowView {
	private TextView mTextView;
	private ActionMode mActionMode;
	public SelectedItemShowView(TextView textView, ActionMode actionMode) {
		mTextView = textView;
		mActionMode = actionMode;
	}
	
	public void update(int count){
		if(mActionMode!=null && mActionMode.isSelectMode()){
			mTextView.setText(String.format(mTextView.getContext().getString(R.string.string_format_item_selected), count));
		}
	}
}
