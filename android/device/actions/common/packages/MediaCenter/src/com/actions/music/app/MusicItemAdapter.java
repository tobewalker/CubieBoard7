package com.actions.music.app;

import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.actions.mediacenter.R;
import com.actions.music.data.LocalMusic;
import com.actions.music.data.MusicItemCache;
import com.actions.music.data.PlayRecord;

public class MusicItemAdapter extends BaseAdapter implements Observer {
	private MusicItemCache mMusicItemCache;
	private PlayRecord mPlayRecord;
	private Context mContext;

	public MusicItemAdapter(Context context) {
		mContext = context;
		mMusicItemCache = MusicItemCache.getInstance(context);
		mMusicItemCache.addObserver(this);
		mPlayRecord = PlayRecord.getInstance(context);
	}

	@Override
	public int getCount() {
		return mMusicItemCache.size();
	}

	@Override
	public Object getItem(int position) {
		return mMusicItemCache.getItem(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.music_playlist_item, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.musicName = (TextView) convertView.findViewById(R.id.music_playlist_item_name);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		LocalMusic music = mMusicItemCache.getItem(position);
		viewHolder.musicName.setText(String.format(mContext.getString(R.string.string_format_music_item), position + 1, music.getName()));
		if (mPlayRecord.getCurrentIndex() == position) {
			convertView.setActivated(true);
		} else {
			convertView.setActivated(false);
		}
		return convertView;
	}

	class ViewHolder {
		TextView musicName;
	}

	@Override
	public void update(Observable observable, Object data) {
		notifyDataSetChanged();
	}

	public void release(){
		mMusicItemCache.deleteObserver(this);
	}
}
