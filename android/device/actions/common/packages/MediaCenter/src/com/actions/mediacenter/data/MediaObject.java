package com.actions.mediacenter.data;

import android.net.Uri;

public abstract class MediaObject {
	public abstract String getDetails();
	public abstract Uri getUri();
	public abstract Path getMediaPath();
}
