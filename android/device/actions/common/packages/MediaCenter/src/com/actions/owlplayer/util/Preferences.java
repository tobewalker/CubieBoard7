package com.actions.owlplayer.util;

import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class Preferences {
	private static String TAG = "Preferences";

	/** Preferences */
	public static final String BROWSER_MODE = "browser_mode";
	public static final String SORT_ORDER = "sort_order";
	public static final String FLOATING_AMOUNT = "floating_amount";
	public static final String WINDOW_SIZE = "window_size";
	public static final String AUDIO_MIXER = "audio_mixer";
	public static final String INSET_AMOUNT = "inset_amount";
	public static final String INSET_SIZE = "inset_size";
	public static final String VIEW_MODE = "view_mode";
	public static final String CYCLE_MODE = "cycle_mode";
	public static final String SUBTITLE_ENABLE = "subtitle_enable";
	public static final String SUBTITLE_SIZE = "subtitle_size";
	public static final String SUBTITLE_COLOR = "subtitle_color";
	public static final String DEVELOPER_OPTIONS = "developer_options";
	public static final String BACKGROUND_VISIBLE = "background_visible";
	public static final String KEEP_RATIO = "keep_ratio";
	public static final String SEEK_AND_PLAY = "seek_and_play";
	public static final String REMINDER_DIALOG = "reminder_dialog";
	public static final String PROGRESS_BAR = "progress_bar";
	public static final String DISPLAY_ENHANCE = "display_enhance";
	public static final String POWER_SAVING = "power_saving";
	public static final String VISION_CROP = "vision_crop";
	public static final String VIDEO_DEINTERLACING = "video_deinterlacing";
	public static final String FLOATING_CATEGORY = "floating_category";
	public static final String INSET_CATEGORY = "inset_category";

	/** get preference settings */
	public static Object getPreferences(Context context, String key, Object deft) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		Map<String, ?> settingMap = settings.getAll();
		Object obj = settingMap.get(key);
		return obj != null ? obj : deft;
	}

	public static Map<String, ?> getPreferences(Context context) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		return settings.getAll();
	}

	/** store preference settings */
	public static void setPreferences(Context context, String key, Object value) {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = settings.edit();
		if (value instanceof Boolean) {
			editor.putBoolean(key, (Boolean) value);
		} else if (value instanceof Integer) {
			editor.putInt(key, (Integer) value);
		} else if (value instanceof String) {
			editor.putString(key, (String) value);
		} else {
			Log.e(TAG, "Unexpected type:" + key + "=" + value);
		}
		editor.commit();
	}
}