package com.actions.owlplayer.app;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.OperationApplicationException;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.provider.MediaStore.Video;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.actions.mediacenter.R;
import com.actions.mediacenter.data.ActionMode;
import com.actions.mediacenter.data.MediaSet;
import com.actions.mediacenter.widget.SelectedItemShowView;
import com.actions.owlplayer.app.VideoItemAdapter.OnVideoItemClickListener;
import com.actions.owlplayer.app.VideoItemAdapter.OnVideoItemLongClickListener;
import com.actions.owlplayer.app.VideoSetAdapter.OnVideoSetClickListener;
import com.actions.owlplayer.app.VideoSetAdapter.OnVideoSetLongClickListener;
import com.actions.owlplayer.data.LocalVideo;
import com.actions.owlplayer.data.LocalVideoSet;
import com.actions.owlplayer.data.MediaDatabaseOpenHelper;
import com.actions.owlplayer.data.MediaItem;
import com.actions.owlplayer.data.MediaProvider;
import com.actions.owlplayer.data.VideoSetManager;
import com.actions.owlplayer.player.PlayActivity;
import com.actions.owlplayer.util.ResolverUtils;

public class VideoBrowserActivity extends Activity implements OnClickListener {
	private static final String TAG = VideoBrowserActivity.class.getSimpleName();

	private final static int MESSAGE_REFRESH_VIEW = 0;

	private Context mContext;
	private OWLApplication mApplication;
	private RecyclerView mRecyclerView;
	private VideoSetAdapter mVideoSetAdapter;
	private ImageButton mDeleteButton;
	private TextView mVideoBrowserPathTextView;

	private RefreshMediaListTask mUpdateTask = null;
	private AlertDialog mAlertDialog = null;
	private VideoSetManager mVideoSetManager;
	private MediaSet mCurrentVideoSet;
	private VideoItemAdapter mVideoItemAdapter;
	private ActionMode mActionMode;
	private SelectedItemShowView mSelectedItemShowView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.video_browser_view);
		mContext = this;
		mApplication = (OWLApplication) getApplication();

		mActionMode = new ActionMode();
		mVideoSetManager = VideoSetManager.getInstance(this);
		registerBroadcastReceiver();
		registerContentObserver();
		initView();
		refreshView();
	}

	private void initView() {
		mDeleteButton = (ImageButton) findViewById(R.id.delete_button);
		mDeleteButton.setVisibility(View.INVISIBLE);
		mDeleteButton.setOnClickListener(this);
		mActionMode.setDeleteButton(mDeleteButton);
		mVideoBrowserPathTextView = (TextView) findViewById(R.id.video_browser_path);
		mVideoBrowserPathTextView.setOnClickListener(this);
		mRecyclerView = (RecyclerView) findViewById(R.id.video_browser_recyclerview);
		mVideoBrowserPathTextView.setText(getString(R.string.text_local_video));

		mSelectedItemShowView = new SelectedItemShowView(mVideoBrowserPathTextView, mActionMode);
		mVideoSetManager.setSelectedCountShowView(mSelectedItemShowView);

		mVideoSetAdapter = new VideoSetAdapter(mApplication, mVideoSetManager);
		mVideoSetAdapter.setOnVideoSetClickListener(mVideoSetClickListener);
		mVideoSetAdapter.setOnVideoSetLongClickListener(mOnVideoSetLongClickListener);
		mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));
		mRecyclerView.setAdapter(mVideoSetAdapter);
	}

	private OnVideoSetClickListener mVideoSetClickListener = new OnVideoSetClickListener() {

		@Override
		public void onItemClick(View view, int position) {
			if (mActionMode.isSetClick()) {
				enterItemMode(position);
			} else {
				mVideoSetManager.videoSetSelectClick(position);
				mRecyclerView.getAdapter().notifyItemChanged(position);
			}
		}
	};
	
	private void registerContentObserver(){
		getContentResolver().registerContentObserver(Video.Media.EXTERNAL_CONTENT_URI, false, mContentObserver);
	}
	
	private void unregisterContentObserver(){
		getContentResolver().unregisterContentObserver(mContentObserver);
	}

	private void registerBroadcastReceiver() {
		IntentFilter mediaFilter = new IntentFilter();
		mediaFilter.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
		mediaFilter.addAction(Intent.ACTION_MEDIA_EJECT);
		mediaFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
		mediaFilter.addDataScheme("file");
		registerReceiver(mMediaBroadcastReceiver, mediaFilter);

		IntentFilter selfFilter = new IntentFilter();
		selfFilter.addAction(Config.ACTION_MEDIA_ITEM_CACHE_REFRESHED);
		registerReceiver(mSelfBroadcastReceiver, selfFilter);
	}

	private BroadcastReceiver mMediaBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.d(TAG, "mMediaBroadcastReceiver:" + action);
			if (action.equals(Intent.ACTION_MEDIA_SCANNER_FINISHED)) {
				Runtime.getRuntime().gc();
				refreshView();
			} else if (action.equals(Intent.ACTION_MEDIA_MOUNTED)) {
				if (getExternalCacheDir() != null)
					onStorageReady();
			} else if (action.equals(Intent.ACTION_MEDIA_EJECT)) {
				Runtime.getRuntime().gc();
			}
		}
	};

	protected void onStorageReady() {
		if (mAlertDialog != null) {
			mAlertDialog.dismiss();
			mAlertDialog = null;
			refreshView();
		}
	}

	private BroadcastReceiver mSelfBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.d(TAG, "mSelfBroadcastReceiver:" + action);
			if (action.equals(Config.ACTION_MEDIA_ITEM_CACHE_REFRESHED)) {
				dismissDialog();
				exitSelectMode();
				mVideoSetManager.update();
				if (!mActionMode.isItemMode()) {
					mVideoSetAdapter.notifyDataSetChanged();
				} else {
					if (mCurrentVideoSet.size() != 0) {
						mVideoItemAdapter.notifyDataSetChanged();
					} else {
						enterSetMode();
					}
				}
			}
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		checkExternalStorageState();
	}

	private void checkExternalStorageState() {
		if (getExternalCacheDir() == null) {
			OnCancelListener onCancel = new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					finish();
				}
			};
			DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			};

			AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.HoloSmallDialog).setTitle(R.string.no_external_storage_title)
					.setIcon(R.drawable.ic_dialog_alert).setMessage(R.string.no_external_storage).setNegativeButton(android.R.string.cancel, onClick)
					.setOnCancelListener(onCancel);
			mAlertDialog = builder.show();
		}
	}

	@Override
	protected void onResume() {
		Log.d(TAG, "onResume");
		super.onResume();

	}
	
	@Override
	protected void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy");
		super.onDestroy();
		dismissDialog();
		Config.setInitThumbParams(false);
		unregisterBroadcastReceiver();
		unregisterContentObserver();
	}

	private void unregisterBroadcastReceiver() {
		unregisterReceiver(mMediaBroadcastReceiver);
		unregisterReceiver(mSelfBroadcastReceiver);
	}

	final Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_REFRESH_VIEW:
				mApplication.getMediaItemCache().refresh();

				break;
			default:
				break;
			}
		}
	};

	private void refreshView() {
		if (mUpdateTask != null) {
			mUpdateTask.cancel(true);
			mUpdateTask = null;
		}

		mUpdateTask = new RefreshMediaListTask();
		mUpdateTask.execute();
	}

	private class RefreshMediaListTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			if (isCancelled()) {
				return null;
			}
			updateMediaDatabase();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
		}

		private void updateMediaDatabase() {
			Cursor cursor = getContentResolver().query(MediaProvider.VIDEO_CONTENT_URI, MediaDatabaseOpenHelper.DB_PROJECTION, null, null, null);

			ArrayList<MediaItem> items = getMediaItems(cursor);
			cursor.close();
			if (isCancelled()) {
				return;
			}

			cursor = ResolverUtils.getExternalVideoCursor(VideoBrowserActivity.this);
			if (cursor == null) {
				Log.e(TAG, "cannot get cursor for external videos!");
				return;
			}
			ArrayList<MediaItem> externalItems = ResolverUtils.getMediaItems(VideoBrowserActivity.this, cursor);
			cursor.close();
			if (isCancelled()) {
				return;
			}

			ArrayList<MediaItem> modifyItems = new ArrayList<MediaItem>();
			for (MediaItem item : items) {
				boolean hasItem = false;
				for (MediaItem external : externalItems) {
					if (item.compare(external)) {
						hasItem = true;
						break;
					}
				}
				if (!hasItem) {
					modifyItems.add(item);
				}
			}

			for (MediaItem item : modifyItems) {
				items.remove(item);
			}
			if (isCancelled()) {
				return;
			}

			modifyItems.clear();
			for (MediaItem external : externalItems) {
				boolean hasItem = false;
				for (MediaItem item : items) {
					if (item.compare(external)) {
						hasItem = true;
						break;
					}
				}
				if (!hasItem) {
					modifyItems.add(external);
				}
			}

			for (MediaItem item : modifyItems) {
				items.add(item);
			}
			if (isCancelled()) {
				return;
			}

			ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
			operations.add(ContentProviderOperation.newDelete(MediaProvider.VIDEO_CONTENT_URI).build());
			for (MediaItem item : items) {
				if (isCancelled()) {
					return;
				}
				ContentValues values = item.getContentValues();
				operations.add(ContentProviderOperation.newInsert(MediaProvider.VIDEO_CONTENT_URI).withValues(values).build());
			}

			try {
				getContentResolver().applyBatch(MediaProvider.AUTHORITY, operations);
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (OperationApplicationException e) {
				e.printStackTrace();
			}
		}

	}

	private ArrayList<MediaItem> getMediaItems(Cursor cursor) {
		if (cursor == null)
			return null;

		ArrayList<MediaItem> items = new ArrayList<MediaItem>();
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			MediaItem mediaItem = new LocalVideo(mApplication, cursor);
			items.add(mediaItem);
		}
		return items;
	}

	private void enterSetMode() {
		mActionMode.setMode(ActionMode.MODE_SET_CLICK);
		mVideoBrowserPathTextView.setText(getString(R.string.text_local_video));
		if (mVideoSetAdapter == null) {
			mVideoSetAdapter = new VideoSetAdapter(mApplication, mVideoSetManager);
			mVideoSetAdapter.setOnVideoSetClickListener(mVideoSetClickListener);
			mVideoSetAdapter.setOnVideoSetLongClickListener(mOnVideoSetLongClickListener);
		}
		mRecyclerView.setAdapter(mVideoSetAdapter);
	}

	private void enterItemMode(int position) {
		mActionMode.setMode(ActionMode.MODE_ITEM_CLICK);
		mCurrentVideoSet = mVideoSetManager.get(position);
		((LocalVideoSet) mCurrentVideoSet).setSelectedCountShowView(mSelectedItemShowView);
		mVideoBrowserPathTextView.setText(mCurrentVideoSet.getName());
		mVideoItemAdapter = new VideoItemAdapter(mApplication, mCurrentVideoSet);
		mVideoItemAdapter.setOnVideoItemClickListener(mOnVideoItemClickListener);
		mVideoItemAdapter.setOnVideoItemLongClickListener(mOnVideoItemLongClickListener);
		mRecyclerView.setAdapter(mVideoItemAdapter);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.video_browser_path:
			onBackPressed();
			break;
		case R.id.delete_button:
			showVideoDeleteDialog();
			break;
		default:
			break;
		}
	}

	public void deleteSelectVideo() {
		boolean change = false;
		if (mActionMode.isItemMode()) {
			for (int i = 0; i < mCurrentVideoSet.size(); i++) {
				LocalVideo video = (LocalVideo) mCurrentVideoSet.getItem(i);
				if (video.isSelect()) {
					video.delete();
					change = true;
				}
			}
		} else {
			for (int i = 0; i < mVideoSetManager.size(); i++) {
				LocalVideoSet set = (LocalVideoSet) mVideoSetManager.get(i);
				if (set.isSelect()) {
					set.delete();
					change = true;
				}
			}
		}
		if (change) {
			refreshView();
		}
	}

	private OnVideoItemClickListener mOnVideoItemClickListener = new OnVideoItemClickListener() {

		@Override
		public void onClick(View v, int position) {
			if (mActionMode.isItemClick()) {
				startPlayActivity(position);
			} else {
				((LocalVideoSet) mCurrentVideoSet).videoItemSelectClick(position);
				mRecyclerView.getAdapter().notifyItemChanged(position);
			}
		}
	};

	private void startPlayActivity(int position) {
		Intent intent = new Intent(this, PlayActivity.class);
		Bundle mBundle = new Bundle();
		mBundle.putInt(Config.INTENT_KEY_VIDEO_ITEM_POSITION, position);
		mBundle.putInt(Config.INTENT_KEY_VIDEO_SET_POSITION, getCurrentSetPosition());
		intent.putExtras(mBundle);
		startActivity(intent);
	}

	private int getCurrentSetPosition() {
		for (int i = 0; i < mVideoSetManager.size(); i++) {
			if (mVideoSetManager.get(i).equals(mCurrentVideoSet)) {
				return i;
			}
		}
		return -1;
	}

	private OnVideoSetLongClickListener mOnVideoSetLongClickListener = new OnVideoSetLongClickListener() {

		@Override
		public boolean onItemLongClick(View view, int position) {
			if (mActionMode.isSetClick()) {
				mActionMode.setMode(ActionMode.MODE_SET_SELECT);
				mVideoSetManager.videoSetSelectClick(position);
				mRecyclerView.getAdapter().notifyItemChanged(position);
				return true;
			}
			return false;
		}
	};

	private OnVideoItemLongClickListener mOnVideoItemLongClickListener = new OnVideoItemLongClickListener() {

		@Override
		public boolean onLongClick(View v, int position) {
			if (mActionMode.isItemClick()) {
				mActionMode.setMode(ActionMode.MODE_ITEM_SELECT);
				((LocalVideoSet) mCurrentVideoSet).videoItemSelectClick(position);
				mRecyclerView.getAdapter().notifyItemChanged(position);
				return true;
			}
			return false;
		}
	};

	private void exitSelectMode() {
		if (mActionMode.isItemSelect()) {
			mActionMode.setMode(ActionMode.MODE_ITEM_CLICK);
			((LocalVideoSet) mCurrentVideoSet).clearItemSelect();
			mVideoBrowserPathTextView.setText(mCurrentVideoSet.getName());
		}
		if (mActionMode.isSetSelect()) {
			mActionMode.setMode(ActionMode.MODE_SET_CLICK);
			mVideoSetManager.clearSetSelect();
			mVideoBrowserPathTextView.setText(getString(R.string.text_local_video));
		}
		mRecyclerView.getAdapter().notifyDataSetChanged();
	}

	@Override
	public void onBackPressed() {
		if (mActionMode.isSelectMode()) {
			exitSelectMode();
			return;
		}
		if (mActionMode.isItemMode()) {
			enterSetMode();
			return;
		}
		super.onBackPressed();
	}

	private ContentObserver mContentObserver = new ContentObserver(mHandler) {

		@Override
		public void onChange(boolean selfChange) {
			super.onChange(selfChange);
			refreshView();
		}
		
	};
	
	private void showVideoDeleteDialog(){
		mAlertDialog = new AlertDialog.Builder(this).setView(R.layout.media_item_delete_dialog)
						.create();
		mAlertDialog.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				exitSelectMode();
				mAlertDialog = null;
			}
		});
		mAlertDialog.show();
		ImageButton cancelButton = (ImageButton) mAlertDialog.findViewById(R.id.cancel_button);
		ImageButton positionButton = (ImageButton) mAlertDialog.findViewById(R.id.position_button);
		positionButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				deleteSelectVideo();
				mAlertDialog.dismiss();
			}
		});
		cancelButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mAlertDialog.dismiss();
			}
		});
	}
	
	private void dismissDialog(){
		if(mAlertDialog!=null){
			mAlertDialog.dismiss();
			mAlertDialog = null;
		}
	}
}
