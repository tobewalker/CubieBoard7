package com.actions.image.data;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.MediaStore.Images;
import android.util.Log;

import com.actions.image.app.Config;
import com.actions.mediacenter.utils.CommonUtil;

public class ImageCache {
	private static final String TAG = ImageCache.class.getSimpleName();
	private Context mContext;
	private static ImageCache sImageCache;
	private static final Object mLock = new Object();
	private List<LocalImage> mAllImageList;
	private ImageCache(Context context){
		mContext = context;
		mAllImageList = new ArrayList<LocalImage>();
		update();
		registerObserver();
	}
	
	public static ImageCache getInstance(Context context){
		if(sImageCache==null){
			synchronized (mLock) {
				if(sImageCache==null){
					sImageCache = new ImageCache(context);
				}
			}
		}
		return sImageCache;
	}
	
	public int size(){
		return mAllImageList.size();
	}
	
	public LocalImage get(int position){
		if(CommonUtil.isAvaiablePosition(position, mAllImageList)){
			return mAllImageList.get(position);
		}
		return null;
	}
	
	public void update(){
		Cursor cursor = mContext.getContentResolver().query(Images.Media.EXTERNAL_CONTENT_URI, LocalImage.PROJECTIONS, null, null, null);
		if(cursor==null){
			return;
		}
		mAllImageList.clear();
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			mAllImageList.add(new LocalImage(mContext,cursor));
			cursor.moveToNext();
		}
		cursor.close();
		mContext.sendBroadcast(new Intent(Config.ACTION_IMAGE_CACHE_REFRESH));
	}
	
	private void registerObserver(){
		mContext.getContentResolver().registerContentObserver(Images.Media.EXTERNAL_CONTENT_URI, false, mContentObserver);
	}
	
	private void unregisterObserver(){
		mContext.getContentResolver().registerContentObserver(Images.Media.EXTERNAL_CONTENT_URI, false, mContentObserver);
	}	
	
	private ContentObserver mContentObserver = new ContentObserver(new Handler()) {

		@Override
		public void onChange(boolean selfChange) {
			Log.d(TAG, "onChange");
			update();
		}
		
	};
	
	public void release(){
		unregisterObserver();
	}
}
