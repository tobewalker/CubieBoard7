package com.actions.mediacenter.data;

public abstract class MediaSet {
	public abstract MediaObject getItem(int position);
	public abstract void addItem(MediaObject item);
	public abstract int size();
	public abstract String getPath();
	public abstract String getName();
	public abstract void clear();
}
