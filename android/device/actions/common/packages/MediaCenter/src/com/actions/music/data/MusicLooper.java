package com.actions.music.data;

import android.content.Context;

import com.actions.mediacenter.utils.Preferences;
import com.actions.music.app.Config;

public class MusicLooper {
	private static final int MODE_LOOP = 0;
	private static final int MODE_ORDER = 1;
	private static final int MODE_SINGLE = 2;
	private static final int MODE_RANDOM =3;
	private int mRepeatMode;
	private Context mContext;
	private static final Object mLock = new Object();
	private static MusicLooper sMusicLooper;
	
	private MusicLooper(Context context) {
		mContext = context;
		mRepeatMode = (int) Preferences.getPreferences(context, Config.PREFERENCE_REPEAT_MODE, MODE_LOOP);
	}
	
	public static MusicLooper getInstance(Context context){
		if(sMusicLooper==null){
			synchronized (mLock) {
				if(sMusicLooper==null){
					sMusicLooper = new MusicLooper(context);
				}
			}
		}
		return sMusicLooper;
	}
	
	public void change(){
		mRepeatMode++;
		mRepeatMode = mRepeatMode%4;
	}
	
	public boolean isLoopMode(){
		return mRepeatMode==MODE_LOOP;
	}
	
	public boolean isOrderMode(){
		return mRepeatMode==MODE_ORDER;
	}
	
	public boolean isSingleMode(){
		return mRepeatMode==MODE_SINGLE;
	}
	
	public boolean isRandomMode(){
		return mRepeatMode==MODE_RANDOM;
	}
	
	public void save(){
		Preferences.setPreferences(mContext, Config.PREFERENCE_REPEAT_MODE, mRepeatMode);
	}
}
