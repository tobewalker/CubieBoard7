package com.actions.image.app;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.actions.image.app.ImageItemAdapter.OnImageItemClickListener;
import com.actions.image.app.ImageItemAdapter.OnImageItemLongClickListener;
import com.actions.image.app.ImageSetAdapter.OnImageSetClickListener;
import com.actions.image.app.ImageSetAdapter.OnImageSetLongClickListener;
import com.actions.image.data.ImageCache;
import com.actions.image.data.ImageSetManager;
import com.actions.image.data.LocalImage;
import com.actions.image.data.LocalImageSet;
import com.actions.mediacenter.R;
import com.actions.mediacenter.app.BaseActivity;
import com.actions.mediacenter.data.ActionMode;
import com.actions.mediacenter.widget.SelectedItemShowView;

public class ImageBrowserActivity extends BaseActivity implements OnClickListener {
	private static final String TAG = ImageBrowserActivity.class.getSimpleName();

	private TextView mBrowserPath;
	private RecyclerView mRecyclerView;
	private ImageButton mDeleteButton;

	private ImageCache mImageCache;
	private ImageSetManager mImageSetManager;
	private ImageSetAdapter mImageSetAdapter;
	private ImageItemAdapter mImageItemAdapter;
	private LocalImageSet mCurrentImageSet;
	private ActionMode mActionMode;
	private SelectedItemShowView mSelectedItemShowView;
	private AlertDialog mAlertDialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_browser_view);

		mImageCache = ImageCache.getInstance(this);
		mImageSetManager = ImageSetManager.getInstance(this);
		mImageSetManager.update();
		mImageSetManager.clearSetSelect();
		mActionMode = new ActionMode();
		initView();
		registerBroadcastReceiver();
	}

	private void initView() {
		mBrowserPath = (TextView) findViewById(R.id.image_browser_path);
		mBrowserPath.setOnClickListener(this);
		mRecyclerView = (RecyclerView) findViewById(R.id.image_browser_recyclerview);
		mRecyclerView.setLayoutManager(new GridLayoutManager(this, Config.BROWSER_SPANCOUNT));
		mDeleteButton = (ImageButton) findViewById(R.id.delete_button);
		mDeleteButton.setOnClickListener(this);
		mDeleteButton.setVisibility(View.INVISIBLE);
		mActionMode.setDeleteButton(mDeleteButton);

		mSelectedItemShowView = new SelectedItemShowView(mBrowserPath, mActionMode);
		mImageSetManager.setSelectedCountShowView(mSelectedItemShowView);
		mBrowserPath.setText(getString(R.string.text_local_image));
	}

	@Override
	protected void onResume() {
		Log.d(TAG, "onResume");
		super.onResume();
		if (mImageSetAdapter == null) {
			mImageSetAdapter = new ImageSetAdapter(this);
			mImageSetAdapter.setOnImageSetLongClickListener(mOnImageSetLongClickListener);
			mImageSetAdapter.setOnImageSetClickListener(mOnImageSetClickListener);
			mRecyclerView.setAdapter(mImageSetAdapter);
		}
	}

	@Override
	protected void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "onDestroy");
		super.onDestroy();
		dismissDialog();
		release();
		unregisterBroadcastReceiver();
	}

	private void registerBroadcastReceiver() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_MEDIA_SCANNER_FINISHED);
		registerReceiver(mImageBroadcastReceiver, filter);

		filter = new IntentFilter();
		filter.addAction(Config.ACTION_IMAGE_CACHE_REFRESH);
		registerReceiver(mSelfBroadcastReceiver, filter);
	}

	private void unregisterBroadcastReceiver() {
		unregisterReceiver(mImageBroadcastReceiver);
		unregisterReceiver(mSelfBroadcastReceiver);
	}

	private BroadcastReceiver mImageBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Intent.ACTION_MEDIA_SCANNER_FINISHED)) {
				Log.d(TAG, "onReceive:" + intent.getAction());
				refresh();
			}
		}
	};

	private BroadcastReceiver mSelfBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Config.ACTION_IMAGE_CACHE_REFRESH)) {
				Log.d(TAG, "mSelfBroadcastReceiver");
				refresh();
			}
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.image_browser_path:
			onBackPressed();
			break;
		case R.id.delete_button:
			showImageDeleteDialog();
			break;
		default:
			break;
		}
	}

	private void deleteSelectImage() {
		if (mActionMode.isItemMode()) {
			for (int i = 0; i < mCurrentImageSet.size(); i++) {
				LocalImage image = (LocalImage) mCurrentImageSet.getItem(i);
				if (image.isSelect()) {
					image.delete();
				}
			}
		} else {
			for (int i = 0; i < mImageSetManager.size(); i++) {
				LocalImageSet set = mImageSetManager.get(i);
				if (set.isSelect()) {
					set.delete();
				}
			}
		}
	}

	public void refresh() {
		dismissDialog();
		exitSelectMode();
		mImageSetManager.update();
		if (!mActionMode.isItemMode()) {
			mImageSetAdapter.notifyDataSetChanged();
		} else {
			if (mCurrentImageSet.size() == 0) {
				enterSetMode();
				return;
			}
			mImageItemAdapter.notifyDataSetChanged();
		}
		Log.d(TAG, "item count:" + mRecyclerView.getAdapter().getItemCount());
	}

	private OnImageSetClickListener mOnImageSetClickListener = new OnImageSetClickListener() {

		@Override
		public void onItemClick(View view, int position) {
			if (mActionMode.isSetClick()) {
				mCurrentImageSet = mImageSetManager.get(position);
				mCurrentImageSet.setSelectedCountShowView(mSelectedItemShowView);
				enterItemMode();
			} else if (mActionMode.isSetSelect()) {
				mImageSetManager.videoSetSelectClick(position);
				mImageSetAdapter.notifyItemChanged(position);
			}
		}
	};

	private void enterSetMode() {
		mActionMode.setMode(ActionMode.MODE_SET_CLICK);
		mRecyclerView.setAdapter(mImageSetAdapter);
		mBrowserPath.setText(getString(R.string.text_local_image));
	}

	private void enterItemMode() {
		if (mCurrentImageSet != null) {
			mActionMode.setMode(ActionMode.MODE_ITEM_CLICK);
			mImageItemAdapter = new ImageItemAdapter(mCurrentImageSet);
			mImageItemAdapter.setOnItemClickListener(mOnImageItemClickListener);
			mImageItemAdapter.setOnItemLongClickListener(mOnImageItemLongClickListener);
			mRecyclerView.setAdapter(mImageItemAdapter);
			mBrowserPath.setText(mCurrentImageSet.getName());
		}
	}

	private void release() {
		mImageCache.release();
		Config.setInitThumbParams(false);
	}

	private OnImageItemClickListener mOnImageItemClickListener = new OnImageItemClickListener() {

		@Override
		public void onItemClick(View view, int position) {
			if (mActionMode.isItemClick()) {
				startPlayActivity((LocalImage) mCurrentImageSet.getItem(position));
			} else if (mActionMode.isItemSelect()) {
				mCurrentImageSet.videoItemSelectClick(position);
				mImageItemAdapter.notifyItemChanged(position);
			}
		}
	};

	private OnImageItemLongClickListener mOnImageItemLongClickListener = new OnImageItemLongClickListener() {

		@Override
		public boolean onItemLongClick(View view, int position) {
			if (mActionMode.isItemClick()) {
				mActionMode.setMode(ActionMode.MODE_ITEM_SELECT);
				mCurrentImageSet.videoItemSelectClick(position);
				mImageItemAdapter.notifyItemChanged(position);
				return true;
			}
			return false;
		}
	};

	private OnImageSetLongClickListener mOnImageSetLongClickListener = new OnImageSetLongClickListener() {

		@Override
		public boolean onItemLongClick(View view, int position) {
			if (mActionMode.isSetClick()) {
				mActionMode.setMode(ActionMode.MODE_SET_SELECT);
				mImageSetManager.videoSetSelectClick(position);
				mImageSetAdapter.notifyItemChanged(position);
				return true;
			}
			return false;
		}
	};

	private void startPlayActivity(LocalImage image) {
		Intent intent = new Intent(this, ImagePlayActivity.class);
		Bundle bundle = new Bundle();
		bundle.putInt(Config.INTENT_KEY_IMAGE_ITEM_POSITION, mCurrentImageSet.getPosition(image));
		bundle.putInt(Config.INTENT_KEY_IMAGE_SET_POSITION, mImageSetManager.getPosition(mCurrentImageSet));
		intent.putExtras(bundle);
		startActivity(intent);
	}

	@Override
	public void onBackPressed() {
		if (mActionMode.isSelectMode()) {
			exitSelectMode();
			return;
		}
		if (mActionMode.isItemMode()) {
			enterSetMode();
			return;
		}
		super.onBackPressed();
	}

	private void exitSelectMode() {
		if (mActionMode.isItemSelect()) {
			mActionMode.setMode(ActionMode.MODE_ITEM_CLICK);
			mCurrentImageSet.clearItemSelect();
			mBrowserPath.setText(mCurrentImageSet.getName());
		}
		if (mActionMode.isSetSelect()) {
			mActionMode.setMode(ActionMode.MODE_SET_CLICK);
			mImageSetManager.clearSetSelect();
			mBrowserPath.setText(getString(R.string.text_local_image));
		}
		mRecyclerView.getAdapter().notifyDataSetChanged();
	}

	private void showImageDeleteDialog(){
		mAlertDialog = new AlertDialog.Builder(this).setView(R.layout.media_item_delete_dialog)
						.create();
		mAlertDialog.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				exitSelectMode();
				mAlertDialog = null;
			}
		});
		mAlertDialog.show();
		ImageButton cancelButton = (ImageButton) mAlertDialog.findViewById(R.id.cancel_button);
		ImageButton positionButton = (ImageButton) mAlertDialog.findViewById(R.id.position_button);
		positionButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				deleteSelectImage();
				mAlertDialog.dismiss();
			}
		});
		cancelButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mAlertDialog.dismiss();
			}
		});
	}
	
	private void dismissDialog(){
		if(mAlertDialog!=null){
			mAlertDialog.dismiss();
			mAlertDialog = null;
		}
	}
}
