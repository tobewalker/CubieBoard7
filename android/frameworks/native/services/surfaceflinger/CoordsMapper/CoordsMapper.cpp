#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <cutils/log.h>
#include <math.h>

#include "CoordsMapper.h"
#include "LandScapeCoordsMapper.h"
#include "PhoneCoordsMapper.h"

//using namespace android;
namespace ActsVR {

CoordsMapper::CoordsMapper() {

}

CoordsMapper::~CoordsMapper() {

}

CoordsMapper* CoordsMapper::create() {

    CoordsMapper* mapper = NULL;
    mapper = new PhoneCoordMapper();
    return mapper;
}

void CoordsMapper::coordsMapping(float scaleX, float scaleY, float offsetY,
        float offsetX_left, float offsetX_right, float * coordsIn,
        size_t coordsCount, float * coordsOutLeft, float * coordsOutRight) {

    if (coordsCount <= 0)
        return;

    for (size_t i = 0; i < coordsCount; i++) {
        float x = *(coordsIn++);
        float y = *(coordsIn++);

        x *= scaleX;
        y *= scaleY;

        y += offsetY;

        *(coordsOutLeft++) = x + offsetX_left;
        *(coordsOutLeft++) = y;

        *(coordsOutRight++) = x + offsetX_right;
        *(coordsOutRight++) = y;
    }
}

void CoordsMapper::texCoordsMapping(float screenWidth, float screenHight, float resWidth, float resHeight,
        float texWidth, float texHeight, float* posLeft, float* posRight,
        vec2* texCoords, vec2* texCoordsLeft, vec2* texCoordsRight,
        bool* leftVisible, bool* rightVisible, bool vrenabled) {

    bool leftV = true, rightV = true;

    { // left view
        float l = posLeft[0], t = posLeft[1], r = posLeft[6], b = posLeft[3];
        float dl = 0, dt = 0, dr = 0, db = 0;
        float viewP_l = 0, viewP_t = 0;
        float viewP_r = screenWidth / 2, viewP_b = screenHight;
        float tex_dl, tex_dt, tex_dr, tex_db;
        if (l < viewP_l)
            dl = viewP_l - l;
        if (r > viewP_r)
            dr = r - viewP_r;
        if (t < viewP_t)
            dt = viewP_t - t;
        if (b > viewP_b)
            db = b - viewP_b;
        if (dl + dr >= resWidth || dt + db >= resHeight) {
            leftV = false;
            goto no_left;
        }
        tex_dl = dl * texWidth / resWidth;
        tex_dr = dr * texWidth / resWidth;
        tex_dt = dt * texHeight / resHeight;
        tex_db = db * texHeight / resHeight;
        for (size_t i = 0; i < 4; i++) {
            texCoordsLeft[i] = texCoords[i];
        }
        posLeft[0] += dl;
        posLeft[1] += dt;
        posLeft[2] += dl;
        posLeft[3] -= db;
        posLeft[4] -= dr;
        posLeft[5] -= db;
        posLeft[6] -= dr;
        posLeft[7] += dt;

        if (vrenabled) {
            texCoordsLeft[0].x += tex_dl;
            texCoordsLeft[0].y += tex_dt;
            texCoordsLeft[1].x += tex_dl;
            texCoordsLeft[1].y -= tex_db;
            texCoordsLeft[2].x -= tex_dr;
            texCoordsLeft[2].y -= tex_db;
            texCoordsLeft[3].x -= tex_dr;
            texCoordsLeft[3].y += tex_dt;
        } else {
            texCoordsLeft[0].x += tex_dl;
            texCoordsLeft[0].y += tex_dt;
            texCoordsLeft[1].x += tex_dl;
            texCoordsLeft[1].y -= tex_db;
            texCoordsLeft[2].x -= tex_dr + texWidth / 2;
            texCoordsLeft[2].y -= tex_db;
            texCoordsLeft[3].x -= tex_dr + texWidth / 2;
            texCoordsLeft[3].y += tex_dt;
        }

    }
no_left:
    { // right view
        float l = posRight[0], t = posRight[1], r = posRight[6],
                b = posRight[3];
        float dl = 0, dt = 0, dr = 0, db = 0;
        float viewP_l = screenWidth / 2, viewP_t = 0, viewP_r = screenWidth,
                viewP_b = screenHight;
        float tex_dl, tex_dt, tex_dr, tex_db;
        if (l < viewP_l)
            dl = viewP_l - l;
        if (r > viewP_r)
            dr = r - viewP_r;
        if (t < viewP_t)
            dt = viewP_t - t;
        if (b > viewP_b)
            db = b - viewP_b;
        if (dl + dr >= resWidth || dt + db >= resHeight) {
            rightV = false;
            goto out;
        }
        tex_dl = dl * texWidth / resWidth;
        tex_dr = dr * texWidth / resWidth;
        tex_dt = dt * texHeight / resHeight;
        tex_db = db * texHeight / resHeight;
        for (size_t i = 0; i < 4; i++) {
            texCoordsRight[i] = texCoords[i];
        }
        posRight[0] += dl;
        posRight[1] += dt;
        posRight[2] += dl;
        posRight[3] -= db;
        posRight[4] -= dr;
        posRight[5] -= db;
        posRight[6] -= dr;
        posRight[7] += dt;

        if (vrenabled) {
            texCoordsRight[0].x += tex_dl;
            texCoordsRight[0].y += tex_dt;
            texCoordsRight[1].x += tex_dl;
            texCoordsRight[1].y -= tex_db;
            texCoordsRight[2].x -= tex_dr;
            texCoordsRight[2].y -= tex_db;
            texCoordsRight[3].x -= tex_dr;
            texCoordsRight[3].y += tex_dt;
        } else {
            texCoordsRight[0].x += tex_dl + texWidth / 2;
            texCoordsRight[0].y += tex_dt;
            texCoordsRight[1].x += tex_dl + texWidth / 2;
            texCoordsRight[1].y -= tex_db;
            texCoordsRight[2].x -= tex_dr;
            texCoordsRight[2].y -= tex_db;
            texCoordsRight[3].x -= tex_dr;
            texCoordsRight[3].y += tex_dt;
        }
    }

out:
    *leftVisible = leftV;
    *rightVisible = rightV;
}

};// namespace ActsVR
