#ifndef __PHONE_COORDS_MAPPER_H__
#define __PHONE_COORDS_MAPPER_H__

#include <stdint.h>
#include <sys/types.h>
#include <string.h>

#include <utils/Thread.h>

#include "Transform.h"
#include "CoordsMapper.h"

using namespace android;

namespace ActsVR {

// Default Coords translator
class PhoneCoordMapper: public CoordsMapper {
    int mRot;
public:
    PhoneCoordMapper();

protected:
    virtual ~PhoneCoordMapper();
    void virtual setOrientation(uint32_t orientation);
    void virtual mappingScreen2Virtual(float screenWidth, float screenHeight,
            vec2* screenPositions, float* virtualPositions,
            uint32_t rotation);
    void virtual mappingVirtual2Screen(float screenWidth, float screenHeight,
            float* virtualPositions, vec2* screenPositions,
            uint32_t rotation);
};

};// namespace ActsVR

#endif //__PHONE_COORDS_MAPPER_H__
