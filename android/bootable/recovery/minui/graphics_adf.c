/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#include <sys/cdefs.h>
#include <sys/mman.h>

#include <adf/adf.h>

//ActionsCode(author:lipeng, type:newcode, comment:the headers for sync_wait)
#include <linux/sync.h>
#include <linux/sw_sync.h>
#include <sys/ioctl.h>
// end ActionsCode

#include "graphics.h"
#include "cutils/properties.h"

#define MAX_INTERFACE_NUM 2

struct adf_surface_pdata {
    GRSurface base;
    int fd;
    //ActionsCode(author:lipeng, type:newcode, comment:remember fence_fd)
    int fence_fd;
    // end ActionsCode
    __u32 offset;
    __u32 pitch;
};

struct adf_interface_info {
	int xres;
	int yres;
};
struct adf_pdata {
    minui_backend base;
    int intf_num;
    int intf_fds[MAX_INTERFACE_NUM];
    int intf_ids[MAX_INTERFACE_NUM];
    adf_id_t eng_id[MAX_INTERFACE_NUM];
    __u32 format;
//ActionsCode(author:liaotianyang, type:newcode, comment:for rorate buf )
    GRSurface tmp;
// end ActionsCode

    struct adf_device dev;
    struct adf_interface_info intf_info[MAX_INTERFACE_NUM]; 
    
    unsigned int current_surface;
    unsigned int n_surfaces;
    struct adf_surface_pdata surfaces[2];
    int bEnableDoubleDisplayVRMode;
    int bEnableDoubleDisplaySwap;
};

static gr_surface adf_flip(struct minui_backend *backend);
static void adf_blank(struct minui_backend *backend, bool blank);

static int adf_surface_init(struct adf_pdata *pdata,
        struct drm_mode_modeinfo *mode, struct adf_surface_pdata *surf)
{
    memset(surf, 0, sizeof(*surf));

    surf->fd = adf_interface_simple_buffer_alloc(pdata->intf_fds[0], mode->hdisplay,
            mode->vdisplay, pdata->format, &surf->offset, &surf->pitch);
    if (surf->fd < 0)
        return surf->fd;

    //ActionsCode(author:lipeng, type:newcode, comment:init fence_fd)
    surf->fence_fd = -1;
    // end ActionsCode

    surf->base.width = mode->hdisplay;
    surf->base.height = mode->vdisplay;
    surf->base.row_bytes = surf->pitch;
    surf->base.pixel_bytes = (pdata->format == DRM_FORMAT_RGB565) ? 2 : 4;
    	
	
    surf->base.data = mmap(NULL, surf->pitch * surf->base.height, PROT_WRITE,
            MAP_SHARED, surf->fd, surf->offset);
    if (surf->base.data == MAP_FAILED) {
        close(surf->fd);
        return -errno;
    }

    return 0;
}
//ActionsCode(author:liaotianyang, type:newcode, comment:for rorate buf )
static int adf_tmp_buf_init(struct adf_pdata *pdata)
{
    GRSurface *pbase = &pdata->surfaces[0].base;
    memcpy(&pdata->tmp, pbase, sizeof(GRSurface));
    pdata->tmp.data = (unsigned char*) malloc(pbase->height * pbase->row_bytes);
    if (!pdata->tmp.data) {
        fprintf(stderr, "failed to allocate in-memory surface\n");
        return -1;
    }
    memset(pdata->tmp.data, 0, pbase->height * pbase->row_bytes);
    return 0;
}
// end ActionsCode
static int adf_interface_init(struct adf_pdata *pdata)
{
    struct adf_interface_data intf_data;
    int ret = 0;
    int err;
	int i ;
	for(i = 0 ; i < pdata->intf_num; i++){
		err = adf_get_interface_data(pdata->intf_fds[i], &intf_data);
	    if (err < 0)
	        return err;
	    pdata->intf_info[i].xres = intf_data.current_mode.hdisplay;
	    pdata->intf_info[i].yres = intf_data.current_mode.vdisplay;
	    if((intf_data.flags & ADF_INTF_FLAG_PRIMARY) != 0){	    
		    
		    err = adf_surface_init(pdata, &intf_data.current_mode, &pdata->surfaces[0]);
		    if (err < 0) {
		        fprintf(stderr, "allocating surface 0 failed: %s\n", strerror(-err));
		        ret = err;
		        goto done;
		    }
		
		    err = adf_surface_init(pdata, &intf_data.current_mode,
		            &pdata->surfaces[1]);
		    if (err < 0) {
		        fprintf(stderr, "allocating surface 1 failed: %s\n", strerror(-err));
		        memset(&pdata->surfaces[1], 0, sizeof(pdata->surfaces[1]));
		        pdata->n_surfaces = 1;
		    } else {
		        pdata->n_surfaces = 2;
		    }
		}
	    
	}
done:
    adf_free_interface_data(&intf_data);
    return ret;
}

static int adf_device_init(struct adf_pdata *pdata, struct adf_device *dev)
{
    adf_id_t intf_id[MAX_INTERFACE_NUM];
    int intf_fds[MAX_INTERFACE_NUM];
    int err, intf_num , i;

    intf_num = adf_find_simple_post_configuration(dev, &pdata->format, 1, &intf_id[0],
            &pdata->eng_id[0]);
    if (intf_num < 0)
        return intf_num;
    fprintf(stderr,"adf_device_init intf_num %d \n",intf_num);
    for(i = 0 ; i < intf_num ; i++){
	    err = adf_device_attach(dev, pdata->eng_id[i], intf_id[i]);
	    if (err < 0 && err != -EALREADY){
	        return err;
		}
		pdata->intf_ids[i] = intf_id[i];
	    pdata->intf_fds[i] = adf_interface_open(dev, intf_id[i], O_RDWR);	   
	    if (pdata->intf_fds[i] < 0){
	        return pdata->intf_fds[i];
	    }  
	}
	pdata->intf_num = intf_num;
    err = adf_interface_init(pdata);
    if (err < 0) {
    	for(i = 0 ; i < intf_num ; i++){
    		close(pdata->intf_fds[i]);
        	pdata->intf_fds[i] = -1;
    	}      
    }
    return err;
}

static gr_surface adf_init(minui_backend *backend)
{
    struct adf_pdata *pdata = (struct adf_pdata *)backend;
    adf_id_t *dev_ids = NULL;
    ssize_t n_dev_ids, i;
    gr_surface ret;

#if defined(RECOVERY_BGRA)
    pdata->format = DRM_FORMAT_BGRA8888;
#elif defined(RECOVERY_RGBX)
    pdata->format = DRM_FORMAT_RGBX8888;
#else
    pdata->format = DRM_FORMAT_RGB565;
#endif

    n_dev_ids = adf_devices(&dev_ids);
    if (n_dev_ids == 0) {
        return NULL;
    } else if (n_dev_ids < 0) {
        fprintf(stderr, "enumerating adf devices failed: %s\n",
                strerror(-n_dev_ids));
        return NULL;
    }

    for (i = 0; i < n_dev_ids; i++) {
        struct adf_device dev;
	
		pdata->intf_fds[i] = -1;
		
        int err = adf_device_open(dev_ids[i], O_RDWR, &pdata->dev);
        if (err < 0) {
            fprintf(stderr, "opening adf device %u failed: %s\n", dev_ids[i],
                    strerror(-err));
            continue;
        }

        err = adf_device_init(pdata, &pdata->dev);
        if (err < 0)
            fprintf(stderr, "initializing adf device %u failed: %s\n",
                    dev_ids[i], strerror(-err));
        
    }

    free(dev_ids);

    if (pdata->intf_fds[0] < 0)
        return NULL;

    //ActionsCode(author:liaotianyang, type:newcode, comment:for rorate buf )
    if (adf_tmp_buf_init(pdata))
        return -1;
    //ret = adf_flip(backend);
	// end ActionsCode

    //ActionsCode(author:huanghaiyu, type:newcode, comment:now remove )
    //adf_blank(backend, true);
    //adf_blank(backend, false);

    return &pdata->tmp;
}

//ActionsCode(author:lipeng, type:newcode, comment:copy sync_wait from libsync)
static int sync_wait(int fd, int timeout)
{
    __s32 to = timeout;

    return ioctl(fd, SYNC_IOC_WAIT, &to);
}
// end ActionsCode
static int ui32PostID = 0;
static gr_surface adf_flip(struct minui_backend *backend)
{
    struct adf_pdata *pdata = (struct adf_pdata *)backend;
    struct adf_surface_pdata *surf = &pdata->surfaces[pdata->current_surface]; 
    int fence_fd, i ;
    int surface_num;
    unsigned int ui32CustomDataSize;
    
    struct owl_adf_buffer_config_ext * psBufferConfigExt;
	struct owl_adf_post_ext * psPostExt;
	struct adf_buffer_config * psBufferConfig;
	
//ActionsCode(author:liaotianyang, type:newcode, comment:for rorate buf )
    roate_framebuf((char*)surf->base.data, (char*)pdata->tmp.data);
// end ActionsCode

	surface_num = pdata->intf_num;
	
	psBufferConfig = calloc(surface_num,
							sizeof(struct adf_buffer_config));
	if (!psBufferConfig)
	{

		goto err_out;
	}
	
	ui32CustomDataSize  = sizeof(struct owl_adf_buffer_config_ext) *
					  surface_num + sizeof(struct owl_adf_post_ext);
					  
	psPostExt = calloc(1, ui32CustomDataSize);

	if (!psPostExt)
	{
		goto err_free_buffer_config;
	}
	
	psPostExt->flag = 0;
	psPostExt->post_id = ui32PostID ++ ;
	psBufferConfigExt = &psPostExt->bufs_ext[0];
	
	for(i = 0; i < pdata->intf_num ; i++){
		psBufferConfig[i].w =  surf->base.width;
		psBufferConfig[i].h =  surf->base.height;
		psBufferConfig[i].pitch[0] = surf->pitch;
		psBufferConfig[i].fd[0] = surf->fd;
		
		psBufferConfig[i].format =  pdata->format;
		psBufferConfig[i].overlay_engine = pdata->eng_id[i];
		psBufferConfig[i].offset[0] = 0;
		psBufferConfig[i].n_planes = 1;
		psBufferConfig[i].acquire_fence = -1;
		
		psBufferConfigExt[i].aintf_id = pdata->intf_ids[i];
	
		/* fill the config_ext */
		psBufferConfigExt[i].stamp = (~(1ULL));
		psBufferConfigExt[i].crop.x1 = 0;
		psBufferConfigExt[i].crop.y1 = 0;
		psBufferConfigExt[i].crop.x2 = surf->base.width;
		psBufferConfigExt[i].crop.y2 = surf->base.height;
		
		if(pdata->bEnableDoubleDisplayVRMode != 0){			
			if(i == pdata->bEnableDoubleDisplaySwap){
				psBufferConfigExt[i].crop.x1 = 0;
				psBufferConfigExt[i].crop.x2 = surf->base.width/2;
			}else{
				psBufferConfigExt[i].crop.x1 = surf->base.width/2;
				psBufferConfigExt[i].crop.x2 = surf->base.width;
			}
		}
	
	
		psBufferConfigExt[i].display.x1 = 0;
		psBufferConfigExt[i].display.y1 = 0;
		psBufferConfigExt[i].display.x2 = pdata->intf_info[i].xres;
		psBufferConfigExt[i].display.y2 = pdata->intf_info[i].yres;
	
	
		psBufferConfigExt[i].transform = 0;
		psBufferConfigExt[i].blend_type = 0;
		psBufferConfigExt[i].plane_alpha = 0xff;
	}

	fence_fd = adf_device_post(&pdata->dev,
	                      (adf_id_t *)&pdata->intf_ids[0], pdata->intf_num,
	                      psBufferConfig, surface_num,
	                      psPostExt, ui32CustomDataSize);
	if (fence_fd < 0)
	{
		goto err_adf_device_post;
	}
	surf->fence_fd = fence_fd;


#if 0 // android original code
    if (fence_fd >= 0)
        close(fence_fd);
#endif

    pdata->current_surface = (pdata->current_surface + 1) % pdata->n_surfaces;

    //ActionsCode(author:lipeng, type:newcode, comment: wait fence)
	if (pdata->surfaces[pdata->current_surface].fence_fd >= 0) {
	        sync_wait(pdata->surfaces[pdata->current_surface].fence_fd, 1000);
	        close(pdata->surfaces[pdata->current_surface].fence_fd);
	        pdata->surfaces[pdata->current_surface].fence_fd = -1;
	}
    // end ActionsCode
//ActionsCode(author:liaotianyang, type:newcode, comment:for rorate buf )
    //return &pdata->surfaces[pdata->current_surface].base;
    return &pdata->tmp;
    
err_adf_device_post:
	free(psPostExt);
err_free_buffer_config:
	free(psBufferConfig);
err_out:
	return &pdata->tmp;
//ActionsCode(author:liaotianyang, type:newcode, comment:for rorate buf )
}

static void adf_blank(struct minui_backend *backend, bool blank)
{
    struct adf_pdata *pdata = (struct adf_pdata *)backend;
    int i;
    for(i = 0 ; i < pdata->intf_num ; i++){
	    adf_interface_blank(pdata->intf_fds[i],
	            blank ? DRM_MODE_DPMS_OFF : DRM_MODE_DPMS_ON);
    }
}

static void adf_surface_destroy(struct adf_surface_pdata *surf)
{
    munmap(surf->base.data, surf->pitch * surf->base.height);
    close(surf->fd);
}

static void adf_exit(struct minui_backend *backend)
{
    struct adf_pdata *pdata = (struct adf_pdata *)backend;
    unsigned int i;

	adf_device_close(&pdata->dev);
	
    for (i = 0; i < pdata->n_surfaces; i++)
        adf_surface_destroy(&pdata->surfaces[i]);
    for( i = 0; i < pdata->intf_num ; i++){
	    if (pdata->intf_fds[i] >= 0)
	        close(pdata->intf_fds[i]);
    }
    free(pdata);
}

minui_backend *open_adf()
{
	char prop_str[PROPERTY_VALUE_MAX];
	prop_str[0] = 0;
	
    struct adf_pdata *pdata = calloc(1, sizeof(*pdata));
    if (!pdata) {
        perror("allocating adf backend failed");
        return NULL;
    }

    pdata->base.init = adf_init;
    pdata->base.flip = adf_flip;
    pdata->base.blank = adf_blank;
    pdata->base.exit = adf_exit;
    
    property_get("owl.hwc.twodisplay.monitorvr", prop_str, "0");    
    pdata->bEnableDoubleDisplayVRMode = atoi(prop_str);
    
    prop_str[0] = '0';    
    property_get("owl.hwc.twodisplay.swap", prop_str, "0");    
    pdata->bEnableDoubleDisplaySwap = atoi(prop_str);
    
    return &pdata->base;
}
