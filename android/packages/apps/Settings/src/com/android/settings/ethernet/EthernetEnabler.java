/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * developed by hclydao@gmail.com
 */
package com.android.settings.ethernet;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.preference.CheckBoxPreference;
import android.net.wifi.WifiManager;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.util.ArrayList;
import com.android.settings.SettingsPreferenceFragment;
import com.android.settings.R;
import com.android.settings.widget.SwitchBar;
import com.android.settings.SettingsActivity;
import android.widget.Switch;
import android.util.Slog;
import android.net.EthernetManager;
import android.provider.Settings;
import android.os.INetworkManagementService;
import android.os.IBinder;
import android.os.ServiceManager;

public class EthernetEnabler implements SwitchBar.OnSwitchChangeListener {
    private final String TAG = "EthernetEnabler";
    private Context mContext;
    private SwitchBar mSwitchBar;
    private String iface, miface, eth_iface;
    private INetworkManagementService mNMService;
	private boolean mListeningToOnSwitchChange = false;
	private EthernetDialog mEthDialog = null;
	private EthernetManager mEthManager;
	private WifiManager mWifiManager;
    public void setConfigDialog(EthernetDialog Dialog) {
        mEthDialog = Dialog;
    }

    public EthernetEnabler(Context context, SwitchBar switchBar,EthernetManager ethernetManager) {
        mContext = context;
        mSwitchBar = switchBar;
		mEthManager = ethernetManager;
		mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        setupSwitchBar();
    }

    public void resume(Context context) {
        mContext = context;
        if (!mListeningToOnSwitchChange) {
            mSwitchBar.addOnSwitchChangeListener(this);
            mListeningToOnSwitchChange = true;
        }
    }

    public void pause() {
        if (mListeningToOnSwitchChange) {
            mSwitchBar.removeOnSwitchChangeListener(this);
            mListeningToOnSwitchChange = false;
        }
    }

    public void setupSwitchBar() {
        if (!mListeningToOnSwitchChange) {
            mSwitchBar.addOnSwitchChangeListener(this);
            mListeningToOnSwitchChange = true;
        }
        mSwitchBar.show();
		int enable = Settings.Global.getInt(mContext.getContentResolver(),Settings.Global.ETHERNET_ON,0);//add by hclydao
		if(enable == EthernetManager.ETHERNET_STATE_ENABLED) {
			mSwitchBar.setChecked(true);
		} else {
			mSwitchBar.setChecked(false);
		}
    }

    public void teardownSwitchBar() {
        if (mListeningToOnSwitchChange) {
            mSwitchBar.removeOnSwitchChangeListener(this);
            mListeningToOnSwitchChange = false;
        }
        mSwitchBar.hide();
    }

    @Override
    public void onSwitchChanged(Switch switchView, boolean isChecked) {
		if(isChecked) {
		Log.i("EthernetEnabler","start to enable ethernet");
			//if(mEthDialog != null)
				//mEthDialog.show();
		mWifiManager.setWifiEnabled(false);
		new Thread(new Runnable() {
                @Override
                public void run() {
                    int enable = Settings.Global.getInt(mContext.getContentResolver(),Settings.Global.ETHERNET_ON,0);
                    while(enable != EthernetManager.ETHERNET_STATE_DISABLED){
                    }
                    Settings.Global.putInt(mContext.getContentResolver(), Settings.Global.ETHERNET_ON, EthernetManager.ETHERNET_STATE_ENABLED);
                }
 		}).start();
		if(mEthManager != null){
                   mEthManager.start();
		}	
		} else {
			if(mEthManager != null)
				mEthManager.stop();
		new Thread(new Runnable() {
                @Override
                public void run() {
                    int enable = Settings.Global.getInt(mContext.getContentResolver(),Settings.Global.ETHERNET_ON,0);
                    while(enable != EthernetManager.ETHERNET_STATE_ENABLED){
                    }
                    Settings.Global.putInt(mContext.getContentResolver(), Settings.Global.ETHERNET_ON, EthernetManager.ETHERNET_STATE_DISABLED);
                }
            }).start();

        IBinder b = ServiceManager.getService(Context.NETWORKMANAGEMENT_SERVICE);
        mNMService = INetworkManagementService.Stub.asInterface(b);

       try {
               final String[] ifaces = mNMService.listInterfaces();//obtain all ifaces, add by bill.
               for (String iface : ifaces) {
                       if(iface.length() > 2){
                               miface = iface.toString().substring(0,3);
                               if(!miface.equals("eth"))//add by bill make sure the interface is ethX 
                                        continue;
				eth_iface =  iface;
                       }
               }
        } catch (Exception e) {
            Log.e(TAG, "Could not get list of interfaces " + e);
        }

		try {
                        Process localProcess = null;
                        localProcess = Runtime.getRuntime().exec("su");
                        OutputStream localOutputStream = localProcess.getOutputStream();
                        DataOutputStream localDataOutputStream = new DataOutputStream(localOutputStream);
			localDataOutputStream.writeBytes("ifconfig " + eth_iface + " down" +"\n");//set ethX down, add by bill.
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
			
		}

    }
}
